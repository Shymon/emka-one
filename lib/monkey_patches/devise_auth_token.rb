raise StandardError, 'This patch is only for devise auth token 1.1.5' unless Gem.loaded_specs["devise_token_auth"].version.to_s == "1.1.5"

module DeviseTokenAuth::Concerns::ResourceFinder
  def find_resource(field, value)
    # Here patch depricated connection_config to connection_db_config
    @resource = if resource_class.try(:connection_db_config).try(:[], :adapter).try(:include?, 'mysql')
                  # fix for mysql default case insensitivity
                  resource_class.where("BINARY #{field} = ? AND provider= ?", value, provider).first
                else
                  resource_class.dta_find_by(field => value, 'provider' => provider)
                end
  end
end
