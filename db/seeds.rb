# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

OWNER_P = 'Pepega123'.freeze

def create_user(first_name:, last_name:, username:, email:, company:, password: OWNER_P, role: :owner)
  user = User.find_or_create_by(
    email: email,
  ) do |u|
    u.first_name = first_name
    u.last_name = last_name
    u.username = username
    u.company = company
    u.password = password
  end
  user.confirm
  user.roles = [Role.find_or_initialize_by(name: Role.role(role))]
  user.save
  user
end

if Rails.env.development? || ENV['SEED_FOR_DEVELOPMENT']
  AdminUser.find_or_create_by(email: 'admin@example.com') do |u|
    u.password = OWNER_P
    u.password_confirmation = OWNER_P
  end
  soryns = Company.find_or_create_by(name: 'Soryns', subdomain: 'soryns')
  create_user(first_name: 'Szymon', last_name: 'R', username: 'shymon', email: 'szymon@soryns.com', company: soryns)
  owner = create_user(first_name: 'Mr. Owner', last_name: 'Sor', username: 'owner', email: 'owner@soryns.com', company: soryns)
  employee = create_user(first_name: 'Mr. Employee', last_name: 'Sor', username: 'employee', email: 'employee@soryns.com', company: soryns, role: :employee)
  absence_reason = soryns.absence_reasons.find_or_create_by(name: 'Opady deszczu')
  construction_site = soryns.construction_sites.find_or_create_by(name: 'Piaskownica ul. Morska')
  client = soryns.clients.find_or_create_by(first_name: 'Paweł', last_name: 'Gaweł')
  material1 = soryns.materials.find_or_create_by(name: 'Cegła', unit: :piece, price: 2.99)
  material2 = soryns.materials.find_or_create_by(name: 'Płytki', unit: :square_meter, price: 12.99)
  soryns.estimations.find_or_create_by!(
    name: 'Dom'
  ) do |estimation|
    estimation.assign_attributes(
      creator: owner,
      client: client,
      estimation_rooms_attributes: [
        {
          name: 'Kuchnia',
          estimation_items_attributes: [
            {
              quantity: 300,
              material: material1
            },
            {
              quantity: 65.5,
              material: material2
            }
          ]
        }
      ]
    )
  end
  soryns.absence_requests.find_or_create_by(
    employee: employee,
    absence_reason: absence_reason
  ) do |absence_request|
    absence_request.assign_attributes(
      from: Time.current,
      to: Time.current + 1.day
    )
  end
  soryns.work_entries.find_or_create_by(
    employee: employee,
    construction_site: construction_site,
    spent_time: 1234
  )
end
