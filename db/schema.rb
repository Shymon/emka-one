# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_03_130135) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "absence_reasons", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.uuid "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_absence_reasons_on_company_id"
    t.index ["name"], name: "index_absence_reasons_on_name"
  end

  create_table "absence_requests", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "note"
    t.date "from"
    t.date "to"
    t.uuid "absence_reason_id", null: false
    t.uuid "company_id", null: false
    t.uuid "employee_id", null: false
    t.integer "status", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["absence_reason_id"], name: "index_absence_requests_on_absence_reason_id"
    t.index ["company_id"], name: "index_absence_requests_on_company_id"
    t.index ["employee_id"], name: "index_absence_requests_on_employee_id"
    t.index ["from"], name: "index_absence_requests_on_from"
    t.index ["status"], name: "index_absence_requests_on_status"
    t.index ["to"], name: "index_absence_requests_on_to"
  end

  create_table "active_admin_comments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.uuid "resource_id"
    t.string "author_type"
    t.uuid "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource"
  end

  create_table "admin_users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "clients", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "company_id", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_clients_on_company_id"
    t.index ["email"], name: "index_clients_on_email"
    t.index ["first_name"], name: "index_clients_on_first_name"
    t.index ["last_name"], name: "index_clients_on_last_name"
    t.index ["phone"], name: "index_clients_on_phone"
  end

  create_table "companies", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "subdomain"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "time_zone", default: "Etc/UTC"
    t.index ["name"], name: "index_companies_on_name"
    t.index ["subdomain"], name: "index_companies_on_subdomain"
  end

  create_table "construction_sites", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.date "investment_time_from"
    t.date "investment_time_to"
    t.boolean "active", default: true, null: false
    t.uuid "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["active"], name: "index_construction_sites_on_active"
    t.index ["company_id"], name: "index_construction_sites_on_company_id"
    t.index ["investment_time_from"], name: "index_construction_sites_on_investment_time_from"
    t.index ["investment_time_to"], name: "index_construction_sites_on_investment_time_to"
    t.index ["name"], name: "index_construction_sites_on_name"
  end

  create_table "estimation_items", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.decimal "quantity", precision: 10, scale: 4
    t.uuid "material_id", null: false
    t.uuid "estimation_room_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["estimation_room_id"], name: "index_estimation_items_on_estimation_room_id"
    t.index ["material_id"], name: "index_estimation_items_on_material_id"
  end

  create_table "estimation_rooms", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.uuid "estimation_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["estimation_id"], name: "index_estimation_rooms_on_estimation_id"
  end

  create_table "estimations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.uuid "client_id", null: false
    t.uuid "creator_id", null: false
    t.uuid "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["client_id"], name: "index_estimations_on_client_id"
    t.index ["company_id"], name: "index_estimations_on_company_id"
    t.index ["creator_id"], name: "index_estimations_on_creator_id"
    t.index ["name"], name: "index_estimations_on_name"
  end

  create_table "materials", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "company_id", null: false
    t.string "name"
    t.integer "unit", null: false
    t.decimal "price", precision: 10, scale: 4
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_materials_on_company_id"
    t.index ["name"], name: "index_materials_on_name"
  end

  create_table "notification_receptions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.datetime "read_at"
    t.string "push_id"
    t.uuid "notification_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["notification_id"], name: "index_notification_receptions_on_notification_id"
    t.index ["read_at"], name: "index_notification_receptions_on_read_at"
    t.index ["user_id"], name: "index_notification_receptions_on_user_id"
  end

  create_table "notifications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "kind"
    t.jsonb "metadata"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "notifiable_type"
    t.uuid "notifiable_id"
    t.index ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable"
  end

  create_table "roles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.uuid "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource"
  end

  create_table "user_devices", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.string "pushy_token"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_user_devices_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "first_name"
    t.string "last_name"
    t.string "username"
    t.uuid "company_id"
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.json "tokens"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_users_on_company_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["first_name"], name: "index_users_on_first_name"
    t.index ["last_name"], name: "index_users_on_last_name"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
    t.index ["username"], name: "index_users_on_username"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "work_entries", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "construction_site_id"
    t.uuid "employee_id", null: false
    t.uuid "absence_reason_id"
    t.uuid "company_id", null: false
    t.integer "spent_time"
    t.text "notes"
    t.datetime "entered_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "day"
    t.index ["absence_reason_id"], name: "index_work_entries_on_absence_reason_id"
    t.index ["company_id"], name: "index_work_entries_on_company_id"
    t.index ["construction_site_id"], name: "index_work_entries_on_construction_site_id"
    t.index ["day"], name: "index_work_entries_on_day"
    t.index ["employee_id"], name: "index_work_entries_on_employee_id"
  end

  add_foreign_key "absence_reasons", "companies"
  add_foreign_key "absence_requests", "absence_reasons"
  add_foreign_key "absence_requests", "companies"
  add_foreign_key "absence_requests", "users", column: "employee_id"
  add_foreign_key "clients", "companies"
  add_foreign_key "construction_sites", "companies"
  add_foreign_key "estimation_items", "estimation_rooms"
  add_foreign_key "estimation_items", "materials"
  add_foreign_key "estimation_rooms", "estimations"
  add_foreign_key "estimations", "clients"
  add_foreign_key "estimations", "companies"
  add_foreign_key "estimations", "users", column: "creator_id"
  add_foreign_key "materials", "companies"
  add_foreign_key "notification_receptions", "notifications"
  add_foreign_key "notification_receptions", "users"
  add_foreign_key "user_devices", "users"
  add_foreign_key "work_entries", "absence_reasons"
  add_foreign_key "work_entries", "companies"
  add_foreign_key "work_entries", "construction_sites"
  add_foreign_key "work_entries", "users", column: "employee_id"
end
