class CreateAbsenceRequests < ActiveRecord::Migration[6.1]
  def change
    create_table :absence_requests, id: :uuid do |t|
      t.string :note
      t.datetime :from
      t.datetime :to
      t.references :absence_reason, null: false, foreign_key: true, type: :uuid, index: true
      t.references :company, null: false, foreign_key: true, type: :uuid, index: true
      t.references :employee, null: false, foreign_key: { to_table: :users }, type: :uuid, index: true
      t.integer :status, null: false

      t.timestamps
    end

    add_index :absence_requests, :from
    add_index :absence_requests, :to
    add_index :absence_requests, :status
  end
end
