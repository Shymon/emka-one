class AddDayToWorkEntries < ActiveRecord::Migration[6.1]
  def change
    add_column :work_entries, :day, :date
    add_index :work_entries, :day
  end
end
