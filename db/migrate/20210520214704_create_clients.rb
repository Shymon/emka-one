class CreateClients < ActiveRecord::Migration[6.1]
  def change
    create_table :clients, id: :uuid do |t|
      t.references :company, null: false, foreign_key: true, type: :uuid, index: true
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone

      t.timestamps
    end

    add_index :clients, :first_name
    add_index :clients, :last_name
    add_index :clients, :email
    add_index :clients, :phone
  end
end
