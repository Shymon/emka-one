class CreateWorkEntries < ActiveRecord::Migration[6.1]
  def change
    create_table :work_entries, id: :uuid do |t|
      t.references :construction_site, foreign_key: true, type: :uuid, index: true
      t.references :employee, null: false, foreign_key: { to_table: :users }, type: :uuid, index: true
      t.references :absence_reason, foreign_key: true, type: :uuid, index: true
      t.references :company, null: false, foreign_key: true, type: :uuid, index: true
      t.integer :spent_time
      t.text :notes
      t.datetime :entered_at

      t.timestamps
    end
  end
end
