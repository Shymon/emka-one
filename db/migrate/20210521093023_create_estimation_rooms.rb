class CreateEstimationRooms < ActiveRecord::Migration[6.1]
  def change
    create_table :estimation_rooms, id: :uuid do |t|
      t.string :name
      t.references :estimation, null: false, foreign_key: true, type: :uuid, index: true

      t.timestamps
    end
  end
end
