class CreateMaterials < ActiveRecord::Migration[6.1]
  def change
    create_table :materials, id: :uuid do |t|
      t.references :company, null: false, foreign_key: true, type: :uuid, index: true
      t.string :name
      t.integer :unit, null: false
      t.decimal :price, precision: 10, scale: 4

      t.timestamps
    end

    add_index :materials, :name
  end
end
