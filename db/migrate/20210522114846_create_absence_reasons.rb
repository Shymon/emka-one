class CreateAbsenceReasons < ActiveRecord::Migration[6.1]
  def change
    create_table :absence_reasons, id: :uuid do |t|
      t.string :name
      t.references :company, null: false, foreign_key: true, type: :uuid, index: true

      t.timestamps
    end

    add_index :absence_reasons, :name
  end
end
