class ChangeToAndFromOfAbsenceRequestsToDate < ActiveRecord::Migration[6.1]
  def change
    change_column :absence_requests, :to, :date
    change_column :absence_requests, :from, :date
  end
end
