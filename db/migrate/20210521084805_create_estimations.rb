class CreateEstimations < ActiveRecord::Migration[6.1]
  def change
    create_table :estimations, id: :uuid do |t|
      t.string :name
      t.references :client, null: false, foreign_key: true, type: :uuid, index: true
      t.references :creator, null: false, foreign_key: { to_table: :users }, type: :uuid, index: true
      t.references :company, null: false, foreign_key: true, type: :uuid, index: true

      t.timestamps
    end

    add_index :estimations, :name
  end
end
