class CreateNotifications < ActiveRecord::Migration[6.1]
  def change
    create_table :notifications, id: :uuid do |t|
      t.references :user, null: false, index: true, foreign_key: true, type: :uuid
      t.timestamp :read_at
      t.string :kind
      t.jsonb :metadata

      t.timestamps
    end

    add_index :notifications, :read_at
  end
end
