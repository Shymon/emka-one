class CleanUpNotifications < ActiveRecord::Migration[6.1]
  def change
    remove_reference :notifications, :user
    remove_column :notifications, :read_at
  end
end
