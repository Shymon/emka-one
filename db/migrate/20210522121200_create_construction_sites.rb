class CreateConstructionSites < ActiveRecord::Migration[6.1]
  def change
    create_table :construction_sites, id: :uuid do |t|
      t.string :name
      t.date :investment_time_from
      t.date :investment_time_to
      t.boolean :active, null: false, default: true
      t.references :company, null: false, foreign_key: true, type: :uuid, index: true

      t.timestamps
    end

    add_index :construction_sites, :name
    add_index :construction_sites, :active
    add_index :construction_sites, :investment_time_from
    add_index :construction_sites, :investment_time_to
  end
end
