class CreateNotificationReceptions < ActiveRecord::Migration[6.1]
  def change
    create_table :notification_receptions, id: :uuid do |t|
      t.references :user, null: false, index: true, foreign_key: true, type: :uuid
      t.timestamp :read_at
      t.string :push_id
      t.references :notification, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end

    add_index :notification_receptions, :read_at
  end
end
