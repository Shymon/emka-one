class AddTimeZoneToCompany < ActiveRecord::Migration[6.1]
  def change
    add_column :companies, :time_zone, :string, default: 'Etc/UTC'
  end
end
