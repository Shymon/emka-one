class CreateEstimationItems < ActiveRecord::Migration[6.1]
  def change
    create_table :estimation_items, id: :uuid do |t|
      t.decimal :quantity, precision: 10, scale: 4
      t.references :material, null: false, foreign_key: true, type: :uuid, index: true
      t.references :estimation_room, null: false, foreign_key: true, type: :uuid, index: true

      t.timestamps
    end
  end
end
