class CreateUserDevices < ActiveRecord::Migration[6.1]
  def change
    create_table :user_devices, id: :uuid do |t|
      t.references :user, null: false, foreign_key: true, type: :uuid, index: true
      t.string :pushy_token

      t.timestamps
    end
  end
end
