Rails.application.reloader.to_prepare do
  Dir[Rails.root.join('lib/monkey_patches/**/*.rb')].each { |f| require f }
end
