Rails.application.routes.draw do
  if ENV['WITH_SWAGGER_DOCS'].presence
    mount Rswag::Ui::Engine => '/api-docs'
    mount Rswag::Api::Engine => '/api-docs'
  end

  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  root to: 'home#index'

  mount_devise_token_auth_for 'User', at: 'api/v1/auth', skip: %i[
    registrations
    token_validations
    omniauth_callbacks
    unlocks
  ], controllers: {
    sessions: 'api/v1/auth/sessions',
  }

  namespace :api do
    namespace :v1 do
      resources :absence_reasons
      resources :absence_requests do
        member do
          put :cancel
        end
      end
      resources :clients
      resources :construction_sites
      resources :estimations do
        resources :estimation_rooms, controller: 'estimations/estimation_rooms' do
          resources :estimation_items, controller: 'estimations/estimation_rooms/estimation_items'
        end
      end
      resources :materials
      resources :users
      resources :work_entries
      resources :notifications, only: %i[index] do
        member do
          put :read
        end
      end
    end
  end

  namespace :workspace do
    get 'dashboard', to: 'dashboard#index'
    devise_for :users, only: %i[sessions confirmations passwords], controllers: {
      sessions: 'devise/sessions',
      confirmations: 'devise/confirmations',
      passwords: 'devise/passwords',
    }
    resources :absence_reasons
    resources :absence_requests do
      member do
        put :accept
        put :reject
        put :cancel
      end
    end
    resources :clients
    resources :construction_sites
    resources :estimations do
      member do
        get :print
      end
    end
    resources :materials
    resources :users
    resources :work_entries
  end

  resources :examples, only: [] do
    collection do
      get :estimation
    end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
