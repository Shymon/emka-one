class Notifications::DispatchNotification
  include Interactor

  def call
    context.notification = Notification.create!(
      notifiable: context.notifiable,
      kind: context.kind.key,
      metadata: context.kind.metadata
    )
    dispatch_standard_notifications
    dispatch_notificaitons_with_push
  end

  private

  def dispatch_standard_notifications
    return unless context.kind.standard_recipients.any?

    context.kind.standard_recipients.each do |recipient|
      context.notification.receptions.create(
        user: recipient
      )
    end
  end

  def dispatch_notificaitons_with_push
    return unless context.kind.push_recipients.any?

    context.kind.push_recipients.each do |recipient|
      context.notification.receptions.create(
        user: recipient
      )
    end
    push_notifications_service.new(
      notification: context.notification,
      notification_receptions: context.notification.receptions
    ).call
  end

  def push_notifications_service
    (context.push_notifications_service || PushNotificationsService)
  end
end
