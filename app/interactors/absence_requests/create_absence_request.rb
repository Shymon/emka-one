class AbsenceRequests::CreateAbsenceRequest
  include Interactor

  def call
    context.absence_request = context.creator.company.absence_requests.create(
      context.params.merge(additional_params)
    )

    if context.absence_request.persisted?
      dispatch_notification unless context.creator.owner?
    else
      context.fail!
    end
  end

  private

  def additional_params
    params = {}
    params.merge!(employee: context.creator) if context.creator.employee?
    params
  end

  def dispatch_notification
    Notifications::DispatchNotification.call(
      notifiable: context.absence_request,
      kind: Notification::AbsenceRequestRequested.new(absence_request: context.absence_request)
    )
  end
end
