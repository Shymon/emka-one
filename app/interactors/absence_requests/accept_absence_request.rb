class AbsenceRequests::AcceptAbsenceRequest
  include Interactor

  def call
    context.absence_request.accept

    if context.absence_request.save
      dispatch_notification
    else
      context.fail!
    end
  end

  private

  def dispatch_notification
    Notifications::DispatchNotification.call(
      notifiable: context.absence_request,
      kind: Notification::AbsenceRequestAccepted.new(absence_request: context.absence_request)
    )
  end
end
