class AbsenceRequests::CancelAbsenceRequest
  include Interactor

  def call
    context.absence_request.cancel

    context.fail! unless context.absence_request.save
  end
end
