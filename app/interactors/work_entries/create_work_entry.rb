class WorkEntries::CreateWorkEntry
  include Interactor

  def call
    context.work_entry = context.creator.company.work_entries.create(
      context.params.merge(additional_params)
    )

    context.fail! unless context.work_entry.persisted?
  end

  private

  def additional_params
    params = {}
    params.merge!(entered_at: Time.current)
    params.merge!(employee: context.creator) if context.creator.employee?
    params
  end
end
