class Users::CreateEmployee
  include Interactor

  def call
    context.user = context.creator.company.users.create(context.params)

    if context.user.persisted?
      context.user.add_role Role.role(:employee)
    else
      context.fail!
    end
  end
end
