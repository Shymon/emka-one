class EstimationDecorator < ApplicationDecorator
  delegate_all

  decorates_association :client

  def to_s
    name
  end

  def created_at
    format_datetime(object.created_at)
  end

  def pdf_options
    {
      margin: {
        top: 0,
        bottom: 0,
        left: 5,
        right: 5,
      },
    }
  end
end
