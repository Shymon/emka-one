class ApplicationDecorator < Draper::Decorator
  # Define methods for all decorated objects.
  # Helpers are accessed through `helpers` (aka `h`). For example:
  #
  #   def percent_amount
  #     h.number_to_percentage object.amount, precision: 2
  #   end

  private

  def format_datetime(datetime)
    datetime&.in_time_zone(company.time_zone)&.strftime(I18n.t('datetime_format'))
  end

  def format_datetime_to_date(datetime)
    datetime&.in_time_zone(company.time_zone)&.strftime(I18n.t('date_format'))
  end

  def format_date(date)
    date&.strftime(I18n.t('date_format'))
  end
end
