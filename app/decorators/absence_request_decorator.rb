class AbsenceRequestDecorator < ApplicationDecorator
  delegate_all

  decorates_association :employee
  decorates_association :absence_reason

  def from
    format_date(object.from)
  end

  def to
    format_date(object.to)
  end
end
