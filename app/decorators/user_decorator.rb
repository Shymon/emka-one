class UserDecorator < ApplicationDecorator
  delegate_all

  def to_s
    [first_name, last_name].join(' ')
  end

  def role
    roles.map { |r| I18n.t("dicts.roles.#{r.name}") }.join(', ')
  end
end
