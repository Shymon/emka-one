class ConstructionSiteDecorator < ApplicationDecorator
  delegate_all

  def to_s
    name
  end

  def investment_time_from
    format_date(object.investment_time_from)
  end

  def investment_time_to
    format_date(object.investment_time_to)
  end
end
