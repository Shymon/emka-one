class WorkEntryDecorator < ApplicationDecorator
  delegate_all

  decorates_association :employee
  decorates_association :construction_site
  decorates_association :absence_reason

  def day
    format_date(object.day)
  end

  def spent_time
    return nil unless object.spent_time

    if object.spent_time > 3600 - 1
      "#{object.spent_time / 3600} #{I18n.t('hours.abbr')} #{object.spent_time % 3600 / 60} #{I18n.t('minutes.abbr')}"
    else
      "#{object.spent_time % 3600 / 60} #{I18n.t('minutes.abbr')}"
    end
  end
end
