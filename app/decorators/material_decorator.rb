class MaterialDecorator < ApplicationDecorator
  delegate_all

  def to_s
    name
  end
end
