class ClientDecorator < ApplicationDecorator
  delegate_all

  def to_s
    [first_name, last_name].join(' ')
  end
end
