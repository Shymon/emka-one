ActiveAdmin.register User do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :email, :first_name, :last_name, :username, :company_id, :password, role_ids: []
  #
  # or
  #
  # permit_params do
  #   permitted = [:email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :first_name, :last_name, :username, :company_id, :provider, :uid, :tokens]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  filter  :last_name
  filter  :first_name
  filter  :username
  filter  :company

  index do
    column :last_name
    column :first_name
    column :username
    column :company
    column :roles
    actions
  end

  form do |f|
    input :last_name
    input :first_name
    input :username
    input :email
    input :company
    input :password
    input :roles, as: :check_boxes

    actions
  end

  controller do
    def update
      model = :user

      if params[model][:password].blank?
        %w(password password_confirmation).each { |p| params[model].delete(p) }
      end

      super
    end
  end
end
