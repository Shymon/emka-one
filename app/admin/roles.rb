ActiveAdmin.register Role do
  permit_params :name

  filter :name

  index do
    column :name
    actions
  end

  form do |f|
    input :name

    actions
  end
end
