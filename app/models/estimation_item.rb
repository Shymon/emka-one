class EstimationItem < ApplicationRecord
  belongs_to :material
  belongs_to :estimation_room

  validates :quantity, presence: true
end
