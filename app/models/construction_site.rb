class ConstructionSite < ApplicationRecord
  belongs_to :company

  validates :name, presence: true, uniqueness: { scope: :company_id }
  validates :investment_time_from, :investment_time_to, presence: true
end
