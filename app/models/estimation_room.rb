class EstimationRoom < ApplicationRecord
  belongs_to :estimation
  has_many :estimation_items, dependent: :destroy
  accepts_nested_attributes_for :estimation_items, reject_if: :all_blank, allow_destroy: true

  validates :name, presence: true
end
