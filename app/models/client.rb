class Client < ApplicationRecord
  belongs_to :company
  has_many :estimations, dependent: :restrict_with_error

  validates :first_name, :last_name, presence: true
end
