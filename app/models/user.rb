class User < ApplicationRecord
  include DeviseTokenAuth::Concerns::User

  rolify
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :confirmable,
         :recoverable, :rememberable, :validatable

  belongs_to :company
  has_many :estimations, dependent: :nullify, inverse_of: :creator, foreign_key: :creator_id
  has_many :work_entries, dependent: :nullify, inverse_of: :employee, foreign_key: :employee_id
  has_many :absence_requests, dependent: :nullify, inverse_of: :employee, foreign_key: :employee_id
  has_many :notification_receptions, dependent: :destroy
  has_many :devices, class_name: 'UserDevice', dependent: :destroy

  validates :company_id, :first_name, :last_name, :username, presence: true
  validates :username, uniqueness: { scope: :company_id }

  after_create -> { confirm }

  def owner?
    @owner ||= has_role? Role.role(:owner)
  end

  def employee?
    @employee ||= has_role? Role.role(:employee)
  end
end
