class UserDevice < ApplicationRecord
  belongs_to :user

  validates :pushy_token, presence: true, uniqueness: true
end
