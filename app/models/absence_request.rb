class AbsenceRequest < ApplicationRecord
  IllegalState = Class.new(StandardError)

  enum status: {
    pending: 10,
    accepted: 20,
    rejected: 30,
    cancelled: 40
  }

  belongs_to :absence_reason
  belongs_to :company
  belongs_to :employee, class_name: 'User', foreign_key: :employee_id

  validates :from, :to, presence: true

  before_validation :set_status

  def accept?
    pending? || rejected?
  end

  def accept
    raise IllegalState unless accept?

    self.status = :accepted
  end

  def reject?
    pending?
  end

  def reject
    raise IllegalState unless reject?

    self.status = :rejected
  end

  def cancel?
    pending?
  end

  def cancel
    raise IllegalState unless cancel?

    self.status = :cancelled
  end

  private

  def set_status
    self.status ||= :pending
  end
end
