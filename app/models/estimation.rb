class Estimation < ApplicationRecord
  belongs_to :client
  belongs_to :creator, class_name: 'User', foreign_key: :creator_id
  belongs_to :company
  has_many :estimation_rooms, dependent: :destroy
  accepts_nested_attributes_for :estimation_rooms, reject_if: :all_blank, allow_destroy: true

  validates :name, presence: true
end
