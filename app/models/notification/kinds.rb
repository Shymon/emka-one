module Notification::Kinds
  class NotificationKind
    extend SmartInit

    def key
      self.class.name.underscore.split('/').last
    end

    def self.key
      name.underscore.split('/').last
    end

    # Recipients with standard notifcation
    def standard_recipients
      []
    end

    # Recipients with standard notifcation and push notification
    def push_recipients
      []
    end

    # Metadata for notification
    def metadata
      {}
    end
  end

  class AbsenceRequestRequested < NotificationKind
    initialize_with :absence_request

    def push_recipients
      absence_request.company.users.with_role(Role.role(:owner))
    end

    def metadata
      { from: absence_request.from, to: absence_request.from, id: absence_request.id }
    end
  end

  class AbsenceRequestAccepted < NotificationKind
    initialize_with :absence_request

    def push_recipients
      [absence_request.employee]
    end

    def metadata
      { from: absence_request.from, to: absence_request.from, id: absence_request.id }
    end
  end

  class AbsenceRequestRejected < NotificationKind
    initialize_with :absence_request

    def push_recipients
      [absence_request.employee]
    end

    def metadata
      { from: absence_request.from, to: absence_request.from, id: absence_request.id }
    end
  end
end
