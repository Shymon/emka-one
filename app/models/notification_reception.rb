class NotificationReception < ApplicationRecord
  belongs_to :notification
  belongs_to :user

  delegate :metadata, :kind, to: :notification, prefix: false

  def read?
    !read_at.nil?
  end
end
