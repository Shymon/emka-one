class Role < ApplicationRecord
  has_and_belongs_to_many :users, join_table: :users_roles

  ROLES = {
    owner: 'owner'.freeze,
    employee: 'employee'.freeze,
  }.freeze

  belongs_to :resource,
             polymorphic: true,
             optional: true

  validates :resource_type,
            inclusion: { in: Rolify.resource_types },
            allow_nil: true

  validates :name, inclusion: { in: ROLES.values }

  scopify

  def self.role(key)
    return Role::ROLES[key] if Role::ROLES[key]

    raise ArgumentError, "Invalid role #{key}"
  end
end
