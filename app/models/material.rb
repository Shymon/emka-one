class Material < ApplicationRecord
  enum unit: {
    piece: 10,
    square_meter: 20,
    running_meter: 30
  }

  belongs_to :company

  validates :name, :unit, :price, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0 }
end
