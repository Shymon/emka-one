class WorkEntry < ApplicationRecord
  belongs_to :construction_site
  belongs_to :employee, class_name: 'User', foreign_key: :employee_id
  belongs_to :absence_reason, optional: true
  belongs_to :company

  validates :spent_time, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :day, presence: true, uniqueness: { scope: %i[company_id employee_id] }
end
