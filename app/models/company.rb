class Company < ApplicationRecord
  has_many :users, dependent: :restrict_with_error
  has_many :materials, dependent: :restrict_with_error
  has_many :clients, dependent: :restrict_with_error
  has_many :estimations, dependent: :restrict_with_error
  has_many :absence_reasons, dependent: :restrict_with_error
  has_many :construction_sites, dependent: :restrict_with_error
  has_many :work_entries, dependent: :restrict_with_error
  has_many :absence_requests, dependent: :restrict_with_error

  validates :name, presence: true, uniqueness: true
end
