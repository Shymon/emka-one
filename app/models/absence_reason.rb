class AbsenceReason < ApplicationRecord
  belongs_to :company
  has_many :absence_requests, dependent: :restrict_with_error

  validates :name, presence: true, uniqueness: { scope: :company_id }
end
