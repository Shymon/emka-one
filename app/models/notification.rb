class Notification < ApplicationRecord
  include Kinds

  KINDS_ARRAY = [
    AbsenceRequestRequested,
    AbsenceRequestAccepted,
    AbsenceRequestRejected
  ].freeze

  belongs_to :notifiable, polymorphic: true, optional: true
  has_many :receptions, dependent: :destroy, class_name: 'NotificationReception'

  validates :kind, inclusion: { in: KINDS_ARRAY.map(&:key) }

  after_initialize -> { self.metadata ||= {} }
end
