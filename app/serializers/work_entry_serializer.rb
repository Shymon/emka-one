class WorkEntrySerializer
  include JSONAPI::Serializer

  attributes :id, :day, :notes, :construction_site_id, :absence_reason_id, :spent_time, :employee_id, :entered_at
end
