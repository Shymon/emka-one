class NotificationReceptionSerializer
  include JSONAPI::Serializer

  attributes :id, :created_at, :read_at, :kind, :metadata
end
