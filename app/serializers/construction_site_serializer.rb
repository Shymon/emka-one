class ConstructionSiteSerializer
  include JSONAPI::Serializer

  attributes :id, :name, :investment_time_from, :investment_time_to, :active
end
