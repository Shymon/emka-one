class EstimationSerializer
  include JSONAPI::Serializer

  attributes :id, :name, :client_id, :creator_id
end
