class AbsenceRequestSerializer
  include JSONAPI::Serializer

  attributes :id, :note, :employee_id, :absence_reason_id, :from, :to, :status
end
