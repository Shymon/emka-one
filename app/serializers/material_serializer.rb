class MaterialSerializer
  include JSONAPI::Serializer

  attributes :id, :name, :unit, :price
end
