class AbsenceReasonSerializer
  include JSONAPI::Serializer

  attributes :id, :name
end
