class EstimationItemSerializer
  include JSONAPI::Serializer

  attributes :id, :quantity, :material_id, :estimation_room_id
end
