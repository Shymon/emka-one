class EstimationRoomSerializer
  include JSONAPI::Serializer

  attributes :id, :name, :estimation_id
end
