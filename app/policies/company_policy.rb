class CompanyPolicy < ApplicationPolicy
  relation_scope do |relation|
    relation.where(id: user.company.id)
  end
end
