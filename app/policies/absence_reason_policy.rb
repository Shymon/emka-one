class AbsenceReasonPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.owner?
  end

  def update?
    user.owner?
  end

  def destroy?
    create?
  end

  relation_scope do |relation|
    next relation.where(company: user.company) if user.owner? || user.employee?

    relation.none
  end

  params_filter do |params|
    params.permit(:name) if user.owner?
  end
end
