class UserPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.owner?
  end

  def update?
    user.owner? || user == record
  end

  def destroy?
    user.owner?
  end

  relation_scope do |relation|
    next user.company.users if user.owner?

    next relation.where(id: user.id) if user.employee?

    relation.none
  end

  params_filter do |params|
    if user.owner?
      params.permit(:first_name, :last_name, :username, :email, :password, :password_confirmation)
    else
      params.permit(:password, :password_confirmation)
    end
  end
end
