class EstimationItemPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.owner?
  end

  def update?
    user.owner?
  end

  def destroy?
    create?
  end

  relation_scope do |relation|
    if user.owner?
      next relation.includes(estimation_room: :estimation).where(estimation_room: { estimations: { company: user.company }})
    end

    relation.none
  end

  params_filter do |params|
    params.permit(:quantity, :material_id) if user.owner?
  end
end
