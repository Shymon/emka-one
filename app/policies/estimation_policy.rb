class EstimationPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.owner?
  end

  def update?
    user.owner?
  end

  def destroy?
    create?
  end

  relation_scope do |relation|
    next relation.where(company: user.company) if user.owner?

    relation.none
  end

  params_filter do |params|
    next unless user.owner?

    params.permit(
      :name,
      :client_id,
      estimation_rooms_attributes: [
        :id,
        :name,
        :_destroy,
        estimation_items_attributes: %i[
          id
          quantity
          material_id
          :_destroy
        ]
      ]
    )
  end
end
