class NotificationReceptionPolicy < ApplicationPolicy
  def index?
    true
  end

  def read?
    user == record.user
  end

  relation_scope do |relation|
    relation.where(user: user)
  end
end
