class AbsenceRequestPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.owner? || user.employee?
  end

  def update?
    user.owner? || (user.employee? && record.pending? && record.employee == user)
  end

  def destroy?
    update?
  end

  def accept?
    user.owner?
  end

  def reject?
    accept?
  end

  def cancel?
    (user.employee? && record.pending? && record.employee == user)
  end

  relation_scope do |relation|
    next relation.where(company: user.company) if user.owner?

    next relation.where(company: user.company, employee: user) if user.employee?

    relation.none
  end

  params_filter do |params|
    next params.permit(:note, :absence_reason_id, :from, :to, :employee_id) if user.owner?
    next params.permit(:note, :absence_reason_id, :from, :to) if user.employee?
  end
end
