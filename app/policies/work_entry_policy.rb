class WorkEntryPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.owner? || user.employee?
  end

  def update?
    user.owner? || (user.employee? && record.employee == user)
  end

  def destroy?
    update?
  end

  relation_scope do |relation|
    next relation.where(company: user.company) if user.owner?

    next relation.where(company: user.company, employee: user) if user.employee?

    relation.none
  end

  params_filter do |params|
    next params.permit(:day, :notes, :construction_site_id, :absence_reason_id, :spent_time, :employee_id) if user.owner?
    next params.permit(:day, :notes, :construction_site_id, :absence_reason_id, :spent_time) if user.employee?
  end
end
