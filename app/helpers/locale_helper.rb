module LocaleHelper
  def t_attribute(klass, attribute)
    klass.human_attribute_name(attribute)
  end
  alias ta t_attribute

  def t_class(klass, count = 1)
    klass.model_name.human count: count
  end
  alias tc t_class

  def t_enum(klass, enum, value)
    I18n.t("dicts.enums.#{klass.name.underscore}.#{enum}.#{value}")
  end
  alias te t_enum

  def t_bool(value)
    value ? I18n.t('yes') : I18n.t('no')
  end
  alias tb t_bool
end
