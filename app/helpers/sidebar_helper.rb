module SidebarHelper
  def sidebar_entry(key)
    if key == :hr
      content_tag(:hr, '', class: 'mr-5')
    else
      link_to(send("workspace_#{key}_path"), class: "font-semibold #{'text-yellow-500' if request.path.include?(key.to_s)}") do
        content_tag(:div, class: 'p-2') do
          [
            content_tag(:div, t(".#{key}.icon"), class: 'w-7 inline-block'),
            content_tag(:span, t(".#{key}.text"))
          ].join.html_safe
        end
      end
    end
  end
end
