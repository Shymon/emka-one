module SelectsHelper
  def enum_for_select(klass, enum)
    klass.send(enum.to_s.pluralize).keys.map do |key|
      [t("dicts.enums.#{klass.name.underscore}.#{enum}.#{key}"), key]
    end
  end
end
