class PushNotificationsService
  extend SmartInit

  initialize_with :notification,
                  :notification_receptions,
                  pushy_token: ENV['PUSHY_TOKEN'],
                  env: Rails.env

  def call
    return if env == 'test'

    Rails.logger.info "DEBUG: Sending push notification #{notification_receptions.inspect}"

    return unless pushy_device_tokens.any?

    PushyAdapter.call(
      token: pushy_token,
      recipients_tokens: pushy_device_tokens,
      notification: notification
    )
  end

  private

  def pushy_device_tokens
    @pushy_device_tokens ||= notification_receptions.map(&:user).flat_map(&:devices).map(&:pushy_token)
  end
end

class PushyAdapter
  def self.call(token:, recipients_tokens:, notification:)
    RestClient.post(
      "https://api.pushy.me/push?api_key=#{token}",
      {
        to: recipients_tokens,
        data: {
          kind: notification.kind,
          metadata: notification.metadata,
        },
      },
      { content_type: :json }
    )
  rescue RestClient::ExceptionWithResponse => e
    # Rollbar.error(e) # TODO: Real erros aggregator
    puts 'ERROR WHEN CONNECTING WITH PUSHY API'
    puts e.response
  end
end
