class PdfGenerationService
  def initialize(service_url: ENV['PDF_GENERATION_SERVICE_URL'], adapter: :wkhtmltopdf)
    @service_url = service_url
    @adapter = adapter
  end

  def call(html_data, gotenberg_options: {}, wkhtmltopdf_options: {})
    case adapter
    when :gotenberg
      raise ArgumentError, 'service_url can\'t be empty' unless service_url.present?

      with_gotenberg_tempfile(html_data) do |file|
        resp = RestClient.post(service_url, { files: [file], **gotenberg_options }, multipart: true)
        resp.body.force_encoding('UTF-8')
      end
    when :wkhtmltopdf
      WickedPdf.new.pdf_from_string(html_data, wkhtmltopdf_options)
    else
      raise NotImplementedError, "Unknown adapter #{adapter}"
    end
  end

  private

  attr_reader :service_url, :adapter

  def with_gotenberg_tempfile(content)
    file = GotenbergTempfile.new
    file.write(content)
    file.rewind
    ret = yield(file)
    file.close
    file.unlink
    ret
  end
end

class GotenbergTempfile < Tempfile
  def original_filename
    'index.html'
  end
end
