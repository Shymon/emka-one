class Workspace::MaterialsController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(Material)

  set_form_attributes(
    [
      :name,
      proc { |ctx, f| f.input :unit, collection: ctx.enum_for_select(Material, :unit) },
      :price
    ]
  )
  set_table_attributes(
    [
      :name,
      [:unit, proc { |ctx, res| ctx.te(Material, :unit, res.unit) }],
      :price
    ]
  )
end
