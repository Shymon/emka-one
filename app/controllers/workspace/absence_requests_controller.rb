class Workspace::AbsenceRequestsController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(AbsenceRequest)

  query_transform { |q| q.includes(:absence_reason, :employee) }

  set_form_attributes(
    [
      proc do |ctx, f|
        f.association(
          :employee,
          collection: UserPolicy.new(user: ctx.current_workspace_user).authorized_scope(User.all).decorate,
          include_blank: false
        )
      end,
      proc do |ctx, f|
        f.association(
          :absence_reason,
          collection: AbsenceReasonPolicy.new(user: ctx.current_workspace_user).authorized_scope(AbsenceReason.all).decorate,
          include_blank: false
        )
      end,
      proc { |ctx, f| f.input :status, collection: ctx.enum_for_select(AbsenceRequest, :status), include_blank: false },
      :from,
      :to,
      :note
    ]
  )
  set_table_attributes(
    [
      :employee,
      :from,
      :to,
      [:status, proc { |ctx, res| ctx.te(AbsenceRequest, :status, res.status) }],
      :absence_reason,
      :note
    ]
  )

  set_additional_table_actions(
    [
      proc do |ctx, res|
        ctx.link_to(
          '✔️',
          ctx.accept_workspace_absence_request_path(res),
          method: :put
        ) if res.accept? && ctx.allowed_to?(:accept?, res.object)
      end,
      proc do |ctx, res|
        ctx.link_to(
          '❌',
          ctx.reject_workspace_absence_request_path(res),
          method: :put
        ) if res.reject? && ctx.allowed_to?(:reject?, res.object)
      end,
      proc do |ctx, res|
        ctx.link_to(
          '✖️',
          ctx.cancel_workspace_absence_request_path(res),
          method: :put
        ) if res.cancel? && ctx.allowed_to?(:cancel?, res.object)
      end
    ]
  )

  create_with(AbsenceRequests::CreateAbsenceRequest)

  def accept
    custom_action_with(AbsenceRequests::AcceptAbsenceRequest)
  end

  def reject
    custom_action_with(AbsenceRequests::RejectAbsenceRequest)
  end

  def cancel
    custom_action_with(AbsenceRequests::CancelAbsenceRequest)
  end
end
