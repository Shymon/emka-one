class Workspace::WorkEntriesController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(WorkEntry)

  query_transform { |q| q.includes(:absence_reason, :employee, :construction_site) }

  set_form_attributes(
    [
      :day,
      proc do |ctx, f|
        f.association(
          :employee,
          collection: UserPolicy.new(user: ctx.current_workspace_user).authorized_scope(User.all).decorate,
          include_blank: false
        )
      end,
      proc do |ctx, f|
        f.association(
          :construction_site,
          collection: ConstructionSitePolicy.new(user: ctx.current_workspace_user).authorized_scope(ConstructionSite.all).decorate,
          include_blank: false
        )
      end,
      proc do |ctx, f|
        f.association(
          :absence_reason,
          collection: AbsenceReasonPolicy.new(user: ctx.current_workspace_user).authorized_scope(AbsenceReason.all).decorate,
          include_blank: false
        )
      end,
      :spent_time,
      :notes
    ]
  )
  set_table_attributes(
    %i[
      day
      employee
      construction_site
      absence_reason
      spent_time
      notes
    ]
  )

  create_with(WorkEntries::CreateWorkEntry)
end
