class Workspace::UsersController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(User)

  query_transform { |q| q.includes(:roles) }

  set_form_attributes %i[first_name last_name username email password password_confirmation]

  set_table_attributes(
    [
      :first_name,
      :last_name,
      :role
    ]
  )

  params_transform do |_, prms|
    if prms[:password].blank?
      prms[:password] = nil
      prms[:password_confirmation] = nil
    end

    prms
  end

  create_with(Users::CreateEmployee)
end
