class Workspace::ClientsController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(Client)

  set_form_attributes %i[first_name last_name email phone]
  set_table_attributes %i[first_name last_name email phone]
end
