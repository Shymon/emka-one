class Workspace::ConstructionSitesController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(ConstructionSite)

  set_form_attributes %i[name investment_time_from investment_time_to active]
  set_table_attributes(
    [
      :name,
      :investment_time_from,
      :investment_time_to,
      [:active, proc { |ctx, res| ctx.tb(res.active) }]
    ]
  )
end
