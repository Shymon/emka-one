class Workspace::BaseController < ApplicationController
  include Pagy::Backend

  before_action :authenticate_workspace_user!
  before_action :load_company

  layout 'workspace'

  responders :flash, :http_cache, :collection
  respond_to :html

  private

  def load_company
    @company = authorized_scope(Company.all).find(current_workspace_user.company_id)
  end

  def current_user
    current_workspace_user
  end
end
