class Workspace::DashboardController < Workspace::BaseController
  decorates_assigned :work_entries
  decorates_assigned :absence_requests

  def index
    @work_entries = authorized_scope(WorkEntry.all)
                    .includes(:absence_reason, :construction_site, :employee).order(created_at: :desc).limit(5)
    @absence_requests = authorized_scope(AbsenceRequest.all)
                        .includes(:absence_reason, :employee).order(created_at: :desc).limit(5)
  end
end
