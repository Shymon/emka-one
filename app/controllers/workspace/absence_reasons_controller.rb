class Workspace::AbsenceReasonsController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(AbsenceReason)

  set_form_attributes %i[name]
  set_table_attributes %i[name]
end
