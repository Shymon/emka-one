class Workspace::EstimationsController < Workspace::BaseController
  include WorkspaceCrud

  workspace_crud(Estimation)

  query_transform { |q| q.includes(:client) }

  set_table_attributes %i[name client created_at]

  set_additional_table_actions([proc { |ctx, res| ctx.link_to '🖨️', ctx.print_workspace_estimation_path(res), target: '_blank' }])

  params_transform do |ctx, prms|
    prms[:creator_id] = ctx.current_workspace_user.id

    prms
  end

  def print
    load_resource
    html = render_to_string 'companies/estimations/report', layout: 'pdf'
    send_data PdfGenerationService.new.call(html, wkhtmltopdf_options: estimation.pdf_options),
              filename: t('workspace.estimations.filename'),
              type: 'application/pdf',
              disposition: 'inline'
  end
end
