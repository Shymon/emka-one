module ApiCrud
  extend ActiveSupport::Concern

  class_methods do
    attr_reader :subject_class, :subject_symbol,
                :params_transform_proc, :query_transform_proc,
                :create_with_interactor_or_proc, :update_with_interactor_or_proc

    def api_crud(klass)
      @subject_class = klass
      @subject_symbol = klass.name.underscore
    end

    def params_transform(&block)
      @params_transform_proc = block
    end

    def query_transform(&block)
      @query_transform_proc = block
    end

    def create_with(interactor_or_proc)
      @create_with_interactor_or_proc = interactor_or_proc
    end

    def update_with(interactor_or_proc)
      @update_with_interactor_or_proc = interactor_or_proc
    end
  end

  included do
    before_action :load_resource, except: %i[index create]

    helper_method :subject_class
    helper_method :subject_symbol
    helper_method :subject_resource
  end

  def index
    resources = authorized_scope(self.class.subject_class.all).order(created_at: :desc)
    resources = query_transform_proc.call(resources) if query_transform_proc
    _, resources = pagy(resources)
    render json: serialized(resources)
  end

  def show
    authorize!

    render json: serialized(subject_resource)
  end

  def create
    authorize!

    if create_with.nil?
      resource = current_user.company.send(subject_symbol.pluralize).create(authorized_scope(subject_resource_params))
      if resource.persisted?
        render json: serialized(resource), status: 201
      else
        render json: { errors: resource.errors }, status: 422
      end
      return
    end

    result =  if create_with.is_a?(Proc)
                create_with.call(self, subject_resource_params)
              else
                create_with.call(
                  params: subject_resource_params,
                  creator: current_user
                )
              end

    if result.success?
      render json: serialized(result.send(subject_symbol)), status: 201
    else
      render json: { errors: result.send(subject_symbol).errors }, status: 422
    end
  end

  def update
    authorize!(subject_resource)

    if update_with.nil?
      if subject_resource.update(authorized_scope(subject_resource_params))
        render json: serialized(subject_resource)
      else
        render json: { errors: subject_resource.errors }, status: 422
      end
      return
    end

    result =  if create_with.is_a?(Proc)
                update_with.call(self, subject_resource, subject_resource_params)
              else
                update_with.call(
                  subject_symbol => subject_resource,
                  params: subject_resource_params,
                  creator: current_user
                )
              end

    if result.success?
      render json: serialized(subject_resource)
    else
      render json: { errors: subject_resource.errors }, status: 422
    end
  end

  def custom_action_with(interactor)
    authorize!(subject_resource)

    result = interactor.call(
      subject_symbol => subject_resource,
      params: subject_resource_params,
      actor: current_user
    )

    if result.success?
      render json: serialized(subject_resource)
    else
      render json: { errors: subject_resource.errors }, status: 422
    end
  end


  def destroy
    authorize!

    if subject_resource.destroy
      render json: serialized(subject_resource)
    else
      render json: { errors: subject_resource.errors }, status: 422
    end
  end

  private

  def subject_class
    self.class.subject_class
  end

  def subject_symbol
    self.class.subject_symbol
  end

  def load_resource
    resource = authorized_scope(self.class.subject_class.all).find(params[:id])
    instance_variable_set(:"@#{subject_symbol}", resource)
  end

  def subject_resource
    instance_variable_get(:"@#{subject_symbol}")
  end

  def query_transform_proc
    self.class.query_transform_proc
  end

  def create_with
    self.class.create_with_interactor_or_proc
  end

  def update_with
    self.class.update_with_interactor_or_proc
  end

  def subject_resource_params
    if self.class.params_transform_proc
      self.class.params_transform_proc.(self, authorized_scope(params[subject_symbol])) 
    else
      authorized_scope(params[subject_symbol])
    end
  end
end
