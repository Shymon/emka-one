module WorkspaceCrud
  extend ActiveSupport::Concern

  class_methods do
    attr_reader :subject_class, :subject_symbol, 
                :params_transform_proc, :query_transform_proc,
                :form_attributes, :table_attributes, :additional_table_actions,
                :create_with_interactor_or_proc, :update_with_interactor_or_proc


    def workspace_crud(klass)
      @subject_class = klass
      @subject_symbol = klass.name.underscore

      decorates_assigned subject_symbol
      decorates_assigned subject_symbol.pluralize
    end

    def params_transform(&block)
      @params_transform_proc = block
    end

    def query_transform(&block)
      @query_transform_proc = block
    end

    def set_form_attributes(attributes)
      @form_attributes = attributes
    end

    def set_table_attributes(attributes)
      @table_attributes = attributes
    end

    def set_additional_table_actions(actions)
      @additional_table_actions = actions
    end

    def create_with(interactor_or_proc)
      @create_with_interactor_or_proc = interactor_or_proc
    end

    def update_with(interactor_or_proc)
      @update_with_interactor_or_proc = interactor_or_proc
    end
  end

  included do
    before_action :load_resource, except: %i[index new create]

    helper_method :subject_class
    helper_method :subject_symbol
    helper_method :subject_resource
    helper_method :form_attributes
    helper_method :table_attributes
    helper_method :additional_table_actions

    rescue_from ActionController::MissingExactTemplate, ActionView::MissingTemplate do |exception|
      render template: "crud/#{exception.try(:path) || action_name}"
    end
  end

  def index
    authorize!

    resources = authorized_scope(self.class.subject_class.all).order(created_at: :desc)
    resources = query_transform_proc.call(resources) if query_transform_proc
    @pagy, resources = pagy(resources)
    instance_variable_set(:"@#{subject_symbol.pluralize}", resources)
  end

  def new
    authorize!

    instance_variable_set(:"@#{subject_symbol}", subject_class.new)
  end

  def create
    if create_with.nil?
      resource = instance_variable_set(
        :"@#{subject_symbol}",
        current_workspace_user.company.send(subject_symbol.pluralize).new(subject_resource_params)
      )
      authorize!(resource)
      resource.save

      respond_with(:workspace, resource, custom_location)
      return
    end

    authorize!
    result = if create_with.is_a?(Proc)
               create_with.call(self, subject_resource_params)
             else
               create_with.call(
                 params: subject_resource_params,
                 creator: current_user
               )
             end
    resource = instance_variable_set(
      :"@#{subject_symbol}",
      result.send(subject_symbol)
    )

    respond_with(:workspace, resource, custom_location)
  end

  def edit; end

  def show
    redirect_to [:edit, :workspace, subject_resource]
  end

  def update
    authorize!(subject_resource)

    if update_with.nil?
      subject_resource.update(subject_resource_params)

      respond_with(:workspace, subject_resource, custom_location)
      return
    end

    if create_with.is_a?(Proc)
      update_with.call(self, subject_resource, subject_resource_params)
    else
      update_with.call(
        subject_symbol => subject_resource,
        params: subject_resource_params,
        creator: current_user
      )
    end

    respond_with(:workspace, subject_resource, custom_location)
  end

  def destroy
    subject_resource.destroy

    respond_with(:workspace, subject_resource, custom_location)
  end

  def custom_action_with(interactor)
    authorize!(subject_resource)

    interactor.call(
      subject_symbol => subject_resource,
      params: subject_resource_params,
      actor: current_user
    )

    respond_with(:workspace, subject_resource, custom_location)
  end

  private

  def subject_class
    self.class.subject_class
  end

  def subject_symbol
    self.class.subject_symbol
  end

  def load_resource
    resource = authorized_scope(self.class.subject_class.all).find(params[:id])
    instance_variable_set(:"@#{subject_symbol}", resource)
  end

  def subject_resource
    instance_variable_get(:"@#{subject_symbol}")
  end

  def subject_resource_params
    if self.class.params_transform_proc
      self.class.params_transform_proc.(self, authorized_scope(params[subject_symbol])) 
    else
      params[subject_symbol] ? authorized_scope(params[subject_symbol]) : {}
    end
  end

  def form_attributes
    self.class.form_attributes
  end

  def table_attributes
    self.class.table_attributes
  end

  def additional_table_actions
    self.class.additional_table_actions
  end

  def query_transform_proc
    self.class.query_transform_proc
  end

  def create_with
    self.class.create_with_interactor_or_proc
  end

  def update_with
    self.class.update_with_interactor_or_proc
  end

  def custom_location
    return {} unless params[:referer]

    { location: params[:referer] }
  end
end
