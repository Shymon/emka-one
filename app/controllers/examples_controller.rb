class ExamplesController < ApplicationController
  def estimation
    @estimation = Estimation.new(
      name: 'Dom',
      client: Client.new(first_name: 'Gaweł', last_name: 'Pawliński'),
      estimation_rooms_attributes: [
        {
          name: 'Kuchnia',
          estimation_items_attributes: [
            {
              quantity: 10.5,
              material: Material.new(name: 'Płytki m²', price: 25.99)
            },
            {
              quantity: 24,
              material: Material.new(name: 'Kafelki m²', price: 55.99)
            }
          ]
        },
        {
          name: 'Sypialnia',
          estimation_items_attributes: [
            {
              quantity: 50,
              material: Material.new(name: 'Panele podłogowe m²', price: 15.99)
            },
            {
              quantity: 2,
              material: Material.new(name: 'Lampa', price: 99.99)
            }
          ]
        },
        {
          name: 'Łazienka',
          estimation_items_attributes: [
            {
              quantity: 2,
              material: Material.new(name: 'Okno', price: 159.99)
            },
            {
              quantity: 10,
              material: Material.new(name: 'Reflektor', price: 66.99)
            },
            {
              quantity: 1,
              material: Material.new(name: 'Grzejnik', price: 300.00)
            }
          ]
        }
      ]
    )

    html = render_to_string 'companies/estimations/report', layout: 'pdf'
    options = {
      margin: {
        top: 0,
        bottom: 0,
        left: 5,
        right: 5
      }
    }
    send_data PdfGenerationService.new.call(html, wkhtmltopdf_options: options),
              filename: 'Wycena.pdf',
              type: 'application/pdf',
              disposition: 'inline'
  end
end
