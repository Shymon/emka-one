class Api::V1::MaterialsController < Api::V1::ApiController
  include ApiCrud

  api_crud(Material)
end
