class Api::V1::Estimations::EstimationRooms::EstimationItemsController < Api::V1::ApiController
  include ApiCrud

  api_crud(EstimationItem)

  before_action :load_estimation
  before_action :load_estimation_room

  def create
    authorize!

    estimation_item = @estimation_room.estimation_items.create(authorized_scope(params.require(:estimation_item)))
    if estimation_item.persisted?
      render json: serialized(estimation_item), status: 201
    else
      render json: { errors: estimation_item.errors }, status: 422
    end
  end

  private

  def load_estimation
    @estimation = authorized_scope(Estimation.all).find(params[:estimation_id])
  end

  def load_estimation_room
    @estimation_room = authorized_scope(EstimationRoom.all).find(params[:estimation_room_id])
  end
end
