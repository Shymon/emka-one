class Api::V1::Estimations::EstimationRoomsController < Api::V1::ApiController
  include ApiCrud

  api_crud(EstimationRoom)

  before_action :load_estimation

  def create
    authorize!

    estimation_room = @estimation.estimation_rooms.create(authorized_scope(params.require(:estimation_room)))
    if estimation_room.persisted?
      render json: serialized(estimation_room), status: 201
    else
      render json: { errors: estimation_room.errors }, status: 422
    end
  end

  private

  def load_estimation
    @estimation = authorized_scope(Estimation.all).find(params[:estimation_id])
  end
end
