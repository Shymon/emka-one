class Api::V1::UsersController < Api::V1::ApiController
  include ApiCrud

  api_crud(User)

  query_transform { |q| q.includes(:roles) }

  params_transform do |_, prms|
    if prms[:password].blank?
      prms[:password] = nil
      prms[:password_confirmation] = nil
    end

    prms
  end

  create_with(Users::CreateEmployee)
end
