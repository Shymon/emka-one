class Api::V1::ClientsController < Api::V1::ApiController
  include ApiCrud

  api_crud(Client)
end
