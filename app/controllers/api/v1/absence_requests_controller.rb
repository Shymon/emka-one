class Api::V1::AbsenceRequestsController < Api::V1::ApiController
  include ApiCrud

  api_crud(AbsenceRequest)

  query_transform { |q| q.includes(:absence_reason, :employee) }

  create_with(AbsenceRequests::CreateAbsenceRequest)

  def cancel
    custom_action_with(AbsenceRequests::CancelAbsenceRequest)
  end
end
