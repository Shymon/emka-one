class Api::V1::ApiController < ApplicationController
  include DeviseTokenAuth::Concerns::SetUserByToken
  include Pagy::Backend

  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!
  before_action :load_company

  respond_to :json

  rescue_from ActiveRecord::RecordNotFound, with: -> { head :not_found }
  rescue_from ActionPolicy::Unauthorized, with: -> { head :forbidden }

  private

  def serialized(objects, options = {})
    object_class = objects.is_a?(Enumerable) ? objects.class.to_s.split('::').first : objects.class.name
    serializer_klass = options.delete(:serializer) || "#{object_class}Serializer".constantize
    serializer_klass.new(objects, options).serializable_hash.to_json
  end

  def load_company
    @company = authorized_scope(Company.all).find(current_user.company_id)
  end
end
