class Api::V1::AbsenceReasonsController < Api::V1::ApiController
  include ApiCrud

  api_crud(AbsenceReason)
end
