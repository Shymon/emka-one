class Api::V1::WorkEntriesController < Api::V1::ApiController
  include ApiCrud

  api_crud(WorkEntry)

  query_transform { |q| q.includes(:absence_reason, :employee, :construction_site) }

  create_with(WorkEntries::CreateWorkEntry)
end
