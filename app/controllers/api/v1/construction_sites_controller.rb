class Api::V1::ConstructionSitesController < Api::V1::ApiController
  include ApiCrud

  api_crud(ConstructionSite)
end
