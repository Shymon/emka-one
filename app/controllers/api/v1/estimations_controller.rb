class Api::V1::EstimationsController < Api::V1::ApiController
  include ApiCrud

  api_crud(Estimation)

  query_transform { |q| q.includes(:client) }

  def create
    authorize!

    estimation_params = authorized_scope(params.require(:estimation))
    estimation_params[:creator_id] = current_user.id
    estimation = current_user.company.estimations.create(estimation_params)
    if estimation.persisted?
      render json: serialized(estimation), status: 201
    else
      render json: { errors: estimation.errors }, status: 422
    end
  end
end
