class Api::V1::Auth::SessionsController < DeviseTokenAuth::SessionsController
  skip_before_action :verify_authenticity_token

  def create
    @pushy_token = params[:pushy_token]
    super
    save_pushy_token if successfully_logged_in
  end

  def destroy
    @pushy_token = params[:pushy_token]
    @was_user = current_user
    super
    remove_pushy_token if successfully_logged_out
  end

  private

  def successfully_logged_in
    response.status == 200 && current_user.present?
  end

  def save_pushy_token
    return unless @pushy_token.present?

    UserDevice.find_by(pushy_token: @pushy_token)&.destroy
    current_user.devices.find_or_create_by(
      pushy_token: @pushy_token
    )
  end

  def successfully_logged_out
    response.status == 200
  end

  def remove_pushy_token
    return unless @pushy_token.present?

    @was_user.devices.find_by(
      pushy_token: @pushy_token
    )&.destroy
  end
end
