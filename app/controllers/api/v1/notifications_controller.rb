class Api::V1::NotificationsController < Api::V1::ApiController
  TIME_LIMIT = 2.weeks

  def index
    resources = authorized_scope(NotificationReception.all)
                .order(created_at: :desc)
                .where('created_at > ?', Time.current - TIME_LIMIT)
    # Maybe add pagination?

    render json: serialized(resources)
  end

  def read
    resource = authorized_scope(NotificationReception.all).find(params[:id])
    resource.update(read_at: Time.current)

    render json: serialized(resource)
  end
end
