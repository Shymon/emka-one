require 'rails_helper'

RSpec.describe Users::CreateEmployee do
  describe 'when user is owner' do
    let!(:user) { create(:user, :owner) }
    let(:params) do
      {
        first_name: 'foo',
        last_name: 'bar',
        username: 'foobar',
        email: 'foo@bar.com',
        password: 'Pepega123'
      }
    end

    it 'creates one user' do
      expect { described_class.call(params: params, creator: user) }.to change { User.count }.by(1)
    end

    it 'creates employee' do
      result = described_class.call(params: params, creator: user)
      expect(result.user.reload.employee?).to be_truthy
    end
  end
end
