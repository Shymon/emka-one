require 'rails_helper'

RSpec.describe WorkEntries::CreateWorkEntry do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:employee) { create(:user, :employee, company: company) }
  let(:params) do
    {
      notes: 'foo',
      day: '29-07-2021',
      construction_site_id: create(:construction_site, company_id: company.id).id,
      spent_time: 331,
      employee_id: employee.id
    }
  end

  describe 'when user is owner' do
    before { update_role :owner }

    it 'creates work entry with given employee' do
      result = described_class.call(params: params, creator: user)
      expect(result.work_entry).to be_persisted
      expect(result.work_entry.employee).to eq(employee)
    end
  end

  describe 'when user is employee' do
    before { update_role :employee }

    it 'creates work entry with user creating it' do
      result = described_class.call(params: params, creator: user)
      expect(result.work_entry).to be_persisted
      expect(result.work_entry.employee).to eq(user)
    end
  end
end
