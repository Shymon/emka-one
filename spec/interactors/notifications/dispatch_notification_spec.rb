require 'rails_helper'

RSpec.describe Notifications::DispatchNotification do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:employee) { create(:user, :employee, company: company) }
  let(:notifiable) { create(:absence_request) }
  let(:standard_recipients) { [] }
  let(:push_recipients) { [] }
  let(:kind) { double }

  let(:push_notifications_service) { double }

  before do
    allow(push_notifications_service).to(receive(:new).and_return(push_notifications_service))
    allow(push_notifications_service).to(receive(:call))

    allow(kind).to receive(:key).and_return(Notification::KINDS_ARRAY.first.key)
    allow(kind).to receive(:metadata).and_return({})
    allow(kind).to receive(:standard_recipients).and_return(standard_recipients)
    allow(kind).to receive(:push_recipients).and_return(push_recipients)
  end

  shared_examples 'does not call push service' do
    it 'does not call push service' do
      expect(push_notifications_service).not_to receive(:call)

      described_class.call(
        notifiable: notifiable,
        kind: kind,
        push_notifications_service: push_notifications_service
      )
    end
  end

  context 'when notification without recipients' do
    include_examples 'does not call push service'

    it 'it creates notification and no receptions' do
      expect do
        described_class.call(
          notifiable: notifiable,
          kind: kind
        )
      end.to change { Notification.count }.by(1).and(
        change { NotificationReception.count }.by(0)
      )
    end
  end

  context 'when notification with standard recipients' do
    let(:standard_recipients) { [user] }

    include_examples 'does not call push service'

    it 'it creates notification and standard recipients' do
      expect do
        described_class.call(
          notifiable: notifiable,
          kind: kind
        )
      end.to change { Notification.count }.by(1).and(
        change { NotificationReception.count }.by(1)
      )
    end

    it 'assigns recipient user to reception' do
      described_class.call(
        notifiable: notifiable,
        kind: kind
      )

      expect(NotificationReception.first.user).to eq(user)
    end
  end

  context 'when notification with push recipients' do
    let(:push_recipients) { [user] }

    it 'it creates notification and standard recipients' do
      expect do
        described_class.call(
          notifiable: notifiable,
          kind: kind,
          push_notifications_service: push_notifications_service
        )
      end.to change { Notification.count }.by(1).and(
        change { NotificationReception.count }.by(1)
      )
    end

    it 'assigns recipient user to reception' do
      described_class.call(
        notifiable: notifiable,
        kind: kind,
        push_notifications_service: push_notifications_service
      )

      expect(NotificationReception.first.user).to eq(user)
    end

    it 'calls push notifications service one' do
      expect(push_notifications_service).to receive(:call).once
      described_class.call(
        notifiable: notifiable,
        kind: kind,
        push_notifications_service: push_notifications_service
      )
    end

    context 'when two push recipients' do
      let(:push_recipients) { [user, employee] }

      it 'calls push notifications service once' do
        expect(push_notifications_service).to receive(:call).once

        described_class.call(
          notifiable: notifiable,
          kind: kind,
          push_notifications_service: push_notifications_service
        )
      end
    end
  end

  # describe 'when user is employee' do
  #   before { update_role :employee }

  #   it 'creates work entry with user creating it' do
  #     result = described_class.call(params: params, creator: user)
  #     expect(result.work_entry).to be_persisted
  #     expect(result.work_entry.employee).to eq(user)
  #   end
  # end
end
