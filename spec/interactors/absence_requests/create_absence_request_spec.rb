require 'rails_helper'

RSpec.describe AbsenceRequests::CreateAbsenceRequest do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:employee) { create(:user, :employee, company: company) }
  let(:params) do
    {
      note: 'foo',
      employee_id: employee.id,
      absence_reason_id: create(:absence_reason, company: company).id,
      from: '08:00 01.03.2021'.to_date.iso8601,
      to: '12:00 01.03.2021'.to_date.iso8601,
    }
  end

  describe 'when user is owner' do
    before { update_role :owner }

    it 'creates work entry with given employee' do
      result = described_class.call(params: params, creator: user)
      expect(result.absence_request).to be_persisted
      expect(result.absence_request.employee).to eq(employee)
    end

    it 'does not send any notification' do
      expect_any_instance_of(Notifications::DispatchNotification).not_to receive(:call)
      described_class.call(params: params, creator: user)
    end
  end

  describe 'when user is employee' do
    before { update_role :employee }

    it 'creates work entry with user creating it' do
      result = described_class.call(params: params, creator: user)
      expect(result.absence_request).to be_persisted
      expect(result.absence_request.employee).to eq(user)
    end

    it 'sends absence request create notification' do
      expect(Notifications::DispatchNotification).to(
        receive(:call).with(
          hash_including(kind: instance_of(Notification::Kinds::AbsenceRequestRequested))
        ).and_call_original
      )
      described_class.call(params: params, creator: user)
    end
  end
end
