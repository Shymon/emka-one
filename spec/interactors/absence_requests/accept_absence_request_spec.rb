require 'rails_helper'

RSpec.describe AbsenceRequests::AcceptAbsenceRequest do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:employee) { create(:user, :employee, company: company) }
  let(:absence_request) { create(:absence_request, employee: employee) }

  describe 'when user is owner' do
    before { update_role :owner }

    it 'creates work entry with given employee' do
      result = described_class.call(absence_request: absence_request)

      expect(result.absence_request.accepted?).to be_truthy
    end

    it 'sends absence request create notification' do
      expect(Notifications::DispatchNotification).to(
        receive(:call).with(
          hash_including(kind: instance_of(Notification::Kinds::AbsenceRequestAccepted))
        ).and_call_original
      )
      described_class.call(absence_request: absence_request)
    end
  end
end
