require 'rails_helper'

RSpec.describe AbsenceRequests::CancelAbsenceRequest do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:employee) { create(:user, :employee, company: company) }
  let(:absence_request) { create(:absence_request, employee: employee) }

  describe 'when user is employee' do
    before { update_role :employee }

    it 'creates work entry with given employee' do
      result = described_class.call(absence_request: absence_request)

      expect(result.absence_request.cancelled?).to be_truthy
    end

    it 'does not send any notification' do
      expect_any_instance_of(Notifications::DispatchNotification).not_to receive(:call)
      described_class.call(absence_request: absence_request)
    end
  end
end
