FactoryBot.define do
  factory :work_entry do
    construction_site
    employee { create(:user) }
    absence_reason
    company
    spent_time { 3600 }
    sequence(:notes) { |n| "Notes #{n}" }
    sequence(:entered_at) { |n| '13:37:01 01.02.2021'.to_datetime + n.days }
    sequence(:day) { |n| '01.02.2021'.to_date + n.days }
  end
end
