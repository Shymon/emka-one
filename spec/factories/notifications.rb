FactoryBot.define do
  factory :notification do
    kind { Notification::Kinds::AbsenceRequestRequested.key }
    metadata { { when: Time.current } }
  end
end
