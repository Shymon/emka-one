FactoryBot.define do
  factory :construction_site do
    sequence(:name) { |n| "ConstructionSite #{n}" }
    investment_time_from { '01.01.2021'.to_date }
    investment_time_to { '01.02.2021'.to_date }
    active { true }
    company
  end
end
