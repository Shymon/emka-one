FactoryBot.define do
  factory :estimation do
    sequence(:name) { |n| "Estimation #{n}" }
    client
    creator { create(:user) }
    company
  end
end
