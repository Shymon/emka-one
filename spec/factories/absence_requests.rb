FactoryBot.define do
  factory :absence_request do
    sequence(:note) { |n| "AbsenceRequest #{n}" }
    from { '08:00 01.01.2021'.to_date }
    to { '12:00 01.01.2021'.to_date }
    absence_reason
    company
    employee { create(:user) }
    status { :pending }
  end
end
