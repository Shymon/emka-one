FactoryBot.define do
  factory :material do
    sequence(:name) { |n| "Material #{n}" }
    unit { Material.units[:piece] }
    price { 213.43 }
    company
  end
end
