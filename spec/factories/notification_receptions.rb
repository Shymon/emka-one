FactoryBot.define do
  factory :notification_reception do
    user
    read_at { Time.current }
    push_id { 'asdf-push-token' }
    notification
  end
end
