FactoryBot.define do
  factory :user_device do
    user
    sequence(:pushy_token) { |n| "nice-pushy-token-#{n}" }
  end
end
