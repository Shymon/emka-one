FactoryBot.define do
  factory :user do
    sequence(:first_name) { |n| "John #{n}" }
    sequence(:last_name) { |n| "Doe #{n}" }
    sequence(:username) { |n| "jdoe#{n}" }
    password { 'Foobar123' }
    sequence(:email) { |n| "johndoe#{n}@example.com" }
    company

    after(:create) do |user|
      user.confirm
    end

    Role::ROLES.each do |role_key, role_name|
      trait role_key do
        roles { [Role.find_or_initialize_by(name: role_name)] }
      end
    end
  end
end
