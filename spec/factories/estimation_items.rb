FactoryBot.define do
  factory :estimation_item do
    quantity { 123.56 }
    material
    estimation_room
  end
end
