FactoryBot.define do
  factory :absence_reason do
    sequence(:name) { |n| "AbsenceReason #{n}" }
    company
  end
end
