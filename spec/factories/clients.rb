FactoryBot.define do
  factory :client do
    sequence(:first_name) { |n| "Client #{n}" }
    sequence(:last_name) { |n| "Clientovich #{n}" }
    sequence(:email) { |n| "client#{n}@example.com" }
    sequence(:phone) { |n| "1234#{n}" }
    company
  end
end
