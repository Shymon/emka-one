FactoryBot.define do
  factory :estimation_room do
    sequence(:name) { |n| "EstimationRoom #{n}" }
    estimation
  end
end
