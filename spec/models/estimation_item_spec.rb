require 'rails_helper'

RSpec.describe EstimationItem, type: :model do
  it 'has valid factory' do
    expect(create(:estimation_item)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:estimation_room) }
  end

  describe 'validations' do
    it { should validate_presence_of(:quantity) }
  end
end
