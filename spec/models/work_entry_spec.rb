require 'rails_helper'

RSpec.describe WorkEntry, type: :model do
  it 'has valid factory' do
    expect(create(:work_entry)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:construction_site) }
    it { should belong_to(:employee) }
    it { should belong_to(:absence_reason).optional }
    it { should belong_to(:company) }
  end

  describe 'validations' do
    it { should validate_presence_of(:spent_time) }
  end
end
