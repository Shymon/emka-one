require 'rails_helper'

RSpec.describe Client, type: :model do
  it 'has valid factory' do
    expect(create(:client)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:company) }
  end
end
