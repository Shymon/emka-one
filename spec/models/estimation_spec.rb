require 'rails_helper'

RSpec.describe Estimation, type: :model do
  it 'has valid factory' do
    expect(create(:estimation)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:company) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end
end
