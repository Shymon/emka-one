require 'rails_helper'

RSpec.describe AbsenceReason, type: :model do
  it 'has valid factory' do
    expect(create(:absence_reason)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:company) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end
end
