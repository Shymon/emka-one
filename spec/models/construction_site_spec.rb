require 'rails_helper'

RSpec.describe ConstructionSite, type: :model do
  it 'has valid factory' do
    expect(create(:construction_site)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:company) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end
end
