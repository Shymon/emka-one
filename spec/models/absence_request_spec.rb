require 'rails_helper'

RSpec.describe AbsenceRequest, type: :model do
  it 'has valid factory' do
    expect(create(:absence_request)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:employee) }
    it { should belong_to(:absence_reason) }
    it { should belong_to(:company) }
  end
end
