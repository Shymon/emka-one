require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has valid factory' do
    expect(create(:user)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:company) }
  end

  describe 'validations' do
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
  end
end
