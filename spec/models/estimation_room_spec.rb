require 'rails_helper'

RSpec.describe EstimationRoom, type: :model do
  it 'has valid factory' do
    expect(create(:estimation_room)).to be_valid
  end

  describe 'associations' do
    it { should belong_to(:estimation) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end
end
