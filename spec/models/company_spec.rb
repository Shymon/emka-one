require 'rails_helper'

RSpec.describe Company, type: :model do
  it 'has valid factory' do
    expect(create(:company)).to be_valid
  end

  describe 'associations' do
    it { should have_many(:users) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end
end
