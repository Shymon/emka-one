require 'workspace_helper'

RSpec.describe 'Workspace/users', type: :request do
  let(:url) { 'users' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:user, :employee, company: company) }
  let(:create_params) do
    {
      user: {
        first_name: 'foo',
        last_name: 'bar',
        username: 'foobar',
        email: 'foo@bar.com',
        password: 'Pepega123'
      }
    }
  end
  let(:update_params) do
    {
      user: {
        first_name: 'foobar'
      }
    }
  end

  include_examples 'workspace_crud', 'users'
end
