require 'workspace_helper'

RSpec.describe 'Workspace/absence_reasons', type: :request do
  let(:url) { 'absence_reasons' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:absence_reason, company: company) }
  let(:create_params) do
    {
      absence_reason: {
        name: 'foo',
      }
    }
  end
  let(:update_params) do
    {
      absence_reason: {
        name: 'foobar'
      }
    }
  end

  include_examples 'workspace_crud', 'absence_reasons'
end
