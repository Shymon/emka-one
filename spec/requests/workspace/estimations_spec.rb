require 'workspace_helper'

RSpec.describe 'Workspace/estimations', type: :request do
  let(:url) { 'estimations' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:estimation, company: company) }
  let(:create_params) do
    {
      estimation: {
        name: 'foo',
        client_id: create(:client, company: company).id
      }
    }
  end
  let(:update_params) do
    {
      estimation: {
        name: 'foobar'
      }
    }
  end

  include_examples 'workspace_crud', 'estimations'
end
