require 'workspace_helper'

RSpec.describe 'Workspace/construction_sites', type: :request do
  let(:url) { 'construction_sites' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:construction_site, company: company) }
  let(:create_params) do
    {
      construction_site: {
        name: 'foo',
        investment_time_from: '01.03.2021'.to_date,
        investment_time_to: '02.03.2021'.to_date
      }
    }
  end
  let(:update_params) do
    {
      construction_site: {
        name: 'foobar'
      }
    }
  end

  include_examples 'workspace_crud', 'construction_sites'
end
