require 'workspace_helper'

RSpec.describe 'Workspace/clients', type: :request do
  let(:url) { 'clients' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:client, company: company) }
  let(:create_params) do
    {
      client: {
        first_name: 'foo',
        last_name: 'bar',
      }
    }
  end
  let(:update_params) do
    {
      client: {
        first_name: 'foobar'
      }
    }
  end

  include_examples 'workspace_crud', 'clients'
end
