require 'workspace_helper'

RSpec.describe 'Workspace/materials', type: :request do
  let(:url) { 'materials' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:material, company: company) }
  let(:create_params) do
    {
      material: {
        name: 'foo',
        unit: :piece,
        price: 12.3
      }
    }
  end
  let(:update_params) do
    {
      material: {
        name: 'foobar'
      }
    }
  end

  include_examples 'workspace_crud', 'materials'
end
