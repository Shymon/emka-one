require 'workspace_helper'

RSpec.describe 'Workspace/absence_requests', type: :request do
  let(:url) { 'absence_requests' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:absence_request, company: company) }
  let(:create_params) do
    {
      absence_request: {
        note: 'foo',
        employee_id: create(:user, :employee, company: company).id,
        absence_reason_id: create(:absence_reason, company: company).id,
        from: '08:00 01.03.2021'.to_date.iso8601,
        to: '12:00 01.03.2021'.to_date.iso8601,
      },
    }
  end
  let(:update_params) do
    {
      absence_request: {
        note: 'foobar',
      },
    }
  end

  include_examples 'workspace_crud', 'absence_requests'

  describe 'PUT workspace/absence_requests/{id}/accept' do
    let(:record) { create(:absence_request, company: company, employee: user) }
    let(:the_request) { put "/workspace/absence_requests/#{record.id}/accept", params: {} }

    it 'calls interactor' do
      sign_in(user, scope: :workspace_user)

      expect(AbsenceRequests::AcceptAbsenceRequest).to receive(:call)
      the_request
    end

    include_examples 'workspace_crud_redirect_to_index'
    include_examples 'workspace_crud_unauthorized'
  end

  describe 'PUT workspace/absence_requests/{id}/reject' do
    let(:record) { create(:absence_request, company: company, employee: user) }
    let(:the_request) { put "/workspace/absence_requests/#{record.id}/reject", params: {} }

    it 'calls interactor' do
      sign_in(user, scope: :workspace_user)

      expect(AbsenceRequests::RejectAbsenceRequest).to receive(:call)
      the_request
    end

    include_examples 'workspace_crud_redirect_to_index'
    include_examples 'workspace_crud_unauthorized'
  end

  describe 'PUT workspace/absence_requests/{id}/cancel' do
    let(:user) { create(:user, :employee, company: company) }
    let(:record) { create(:absence_request, company: company, employee: user) }
    let(:the_request) { put "/workspace/absence_requests/#{record.id}/cancel", params: {} }

    it 'calls interactor' do
      sign_in(user, scope: :workspace_user)

      expect(AbsenceRequests::CancelAbsenceRequest).to receive(:call)
      the_request
    end

    include_examples 'workspace_crud_redirect_to_index'
    include_examples 'workspace_crud_unauthorized'
  end
end
