require 'workspace_helper'

RSpec.describe 'Workspace/work_entries', type: :request do
  let(:url) { 'work_entries' }
  let(:company) { create(:company) }
  let(:user) { create(:user, :owner, company: company) }
  let(:record) { create(:work_entry, company: company) }
  let(:create_params) do
    {
      work_entry: {
        notes: 'foo',
        day: '29-07-2021',
        construction_site_id: create(:construction_site, company_id: company.id).id,
        spent_time: 331,
        employee_id: create(:user, :employee, company_id: company.id).id
      }
    }
  end
  let(:update_params) do
    {
      work_entry: {
        notes: 'foobar'
      }
    }
  end

  include_examples 'workspace_crud', 'work_entries'
end
