require 'swagger_helper'

RSpec.describe 'Clients API at /api/v1/clients' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  client_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        first_name: { type: :string },
        last_name: { type: :string },
        email: { type: :string },
        phone: { type: :string }
      },
      required: %w[id first_name last_name email phone]
    }
  }
  clients_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: client_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  client_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: client_properties_schema,
        required: %w[id]
      }
    }
  }

  path '/api/v1/clients' do
    get 'clients from company' do
      tags 'Clients'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return clients' do
        sign_in(:owner)
        schema(clients_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create client' do
      let(:params) do
        {
          client: {
            first_name: 'foo',
            last_name: 'bar',
            email: 'foo@bar.com',
            phone: '666 666 666',
          }
        }
      end

      tags 'Clients'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          client: {
            properties: {
              first_name: { type: :string },
              last_name: { type: :string },
              email: { type: :string },
              phone: { type: :string }
            },
            required: %w[first_name last_name email phone]
          },
          required: %w[client]
        }
      }

      response '201', 'created client' do
        sign_in(:owner)
        schema(client_schema)

        run_test! do |response|
          expect(Client.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            client: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { first_name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/clients/{id}' do
    let(:client) { create(:client, company: owner.company) }
    let(:id) { client.id }

    get 'client from company' do
      tags 'Clients'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns client' do
        sign_in(:owner)
        schema(client_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates client' do
      tags 'Clients'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          client: {
            properties: {
              first_name: { type: :string },
              last_name: { type: :string },
              email: { type: :string },
              phone: { type: :string }
            }
          },
          required: %w[client]
        }
      }

      response '200', 'updates client' do
        sign_in(:owner)
        schema(client_schema)
        let(:params) do
          {
            client: {
              first_name: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(client.reload.first_name).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            client: {
              first_name: ''
            }
          }
        end
        examples 'application/json' => {
          errors: { first_name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys client' do
      tags 'Clients'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys client' do
        sign_in(:owner)
        schema(client_schema)

        run_test! do
          expect(Client.find_by(id: client.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
