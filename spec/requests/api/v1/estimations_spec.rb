require 'swagger_helper'

RSpec.describe 'Estimations API at /api/v1/estimations' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:client) { create(:client) }
  let(:company_id) { owner.company_id }
  estimation_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        name: { type: :string },
        client_id: { type: :string },
        creator_id: { type: :string }
      },
      required: %w[id name client_id creator_id]
    }
  }
  estimations_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: estimation_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  estimation_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: estimation_properties_schema,
        required: %w[id]
      }
    }
  }
  nested_estimation_attributes = {
    estimation_rooms_attributes: {
      type: :array,
      items: {
        type: :object,
        properties: {
          name: { type: :string },
          estimation_items_attributes: {
            type: :array,
            items: {
              type: :object,
              properties: {
                quantity: { type: :number },
                material_id: { type: :string }
              },
              required: %w[quantity material_id]
            }
          }
        },
        required: %w[name]
      }
    }
  }

  path '/api/v1/estimations' do
    get 'estimations from company' do
      tags 'Estimations'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return estimations' do
        sign_in(:owner)
        schema(estimations_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create estimation' do
      let(:material) { create(:material) }
      let(:params) do
        {
          estimation: {
            name: 'foo',
            client_id: client.id,
            estimation_rooms_attributes: [
              {
                name: 'Living room',
                estimation_items_attributes: [
                  {
                    quantity: 10.5,
                    material_id: material.id
                  }
                ]
              }
            ]
          }
        }
      end

      tags 'Estimations'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          estimation: {
            properties: {
              name: { type: :string },
              client_id: { type: :integer },
              **nested_estimation_attributes
            },
            required: %w[name client_id]
          },
          required: %w[estimation]
        }
      }

      response '201', 'created estimation' do
        sign_in(:owner)
        schema(estimation_schema)

        run_test! do |response|
          estimation = Estimation.joins(estimation_rooms: :estimation_items).find(JSON.parse(response.body)['data']['id'])
          expect(estimation).to be_present
          expect(estimation.estimation_rooms.count).to eq(1)
          expect(estimation.estimation_rooms.first.estimation_items.count).to eq(1)
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            estimation: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/estimations/{id}' do
    let(:estimation) { create(:estimation, company: owner.company) }
    let(:id) { estimation.id }

    get 'estimation from company' do
      tags 'Estimations'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns estimation' do
        sign_in(:owner)
        schema(estimation_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates estimation' do
      tags 'Estimations'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          estimation: {
            properties: {
              name: { type: :string },
              client_id: { type: :string },
              **nested_estimation_attributes
            }
          },
          required: %w[estimation]
        }
      }

      response '200', 'updates estimation' do
        sign_in(:owner)
        schema(estimation_schema)
        let(:params) do
          {
            estimation: {
              name: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(estimation.reload.name).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            estimation: {
              name: ''
            }
          }
        end
        examples 'application/json' => {
          errors: { name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys estimation' do
      tags 'Estimations'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys estimation' do
        sign_in(:owner)
        schema(estimation_schema)

        run_test! do
          expect(Estimation.find_by(id: estimation.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
