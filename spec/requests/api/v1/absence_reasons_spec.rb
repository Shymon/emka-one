require 'swagger_helper'

RSpec.describe 'AbsenceReasons API at /api/v1/absence_reasons' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  absence_reason_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        name: { type: :string },
      },
      required: %w[id name]
    }
  }
  absence_reasons_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: absence_reason_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  absence_reason_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: absence_reason_properties_schema,
        required: %w[id]
      }
    }
  }

  path '/api/v1/absence_reasons' do
    get 'absence_reasons from company' do
      tags 'AbsenceReasons'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return absence_reasons' do
        sign_in(:owner)
        schema(absence_reasons_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create absence_reason' do
      let(:params) do
        {
          absence_reason: {
            name: 'foo',
          }
        }
      end

      tags 'AbsenceReasons'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          absence_reason: {
            properties: {
              name: { type: :string },
            },
            required: %w[name]
          },
          required: %w[absence_reason]
        }
      }

      response '201', 'created absence_reason' do
        sign_in(:owner)
        schema(absence_reason_schema)

        run_test! do |response|
          expect(AbsenceReason.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            absence_reason: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/absence_reasons/{id}' do
    let(:absence_reason) { create(:absence_reason, company: owner.company) }
    let(:id) { absence_reason.id }

    get 'absence_reason from company' do
      tags 'AbsenceReasons'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns absence_reason' do
        sign_in(:owner)
        schema(absence_reason_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates absence_reason' do
      tags 'AbsenceReasons'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          absence_reason: {
            properties: {
              name: { type: :string },
            }
          },
          required: %w[absence_reason]
        }
      }

      response '200', 'updates absence_reason' do
        sign_in(:owner)
        schema(absence_reason_schema)
        let(:params) do
          {
            absence_reason: {
              name: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(absence_reason.reload.name).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            absence_reason: {
              name: ''
            }
          }
        end
        examples 'application/json' => {
          errors: { name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys absence_reason' do
      tags 'AbsenceReasons'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys absence_reason' do
        sign_in(:owner)
        schema(absence_reason_schema)

        run_test! do
          expect(AbsenceReason.find_by(id: absence_reason.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
