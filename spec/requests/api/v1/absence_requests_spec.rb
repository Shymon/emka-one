require 'swagger_helper'

RSpec.describe 'AbsenceRequests API at /api/v1/absence_requests' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  absence_request_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        note: { type: :string },
        employee_id: { type: :string },
        absence_reason_id: { type: :string },
        from: { type: :string },
        to: { type: :string },
        status: { type: :string },
      },
      required: %w[
        id
        note
        employee_id
        absence_reason_id
        from
        to
        status
      ],
    },
  }
  absence_requests_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: absence_request_properties_schema,
          required: %w[id],
        },
      },
    },
  }
  absence_request_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: absence_request_properties_schema,
        required: %w[id],
      },
    },
  }

  path '/api/v1/absence_requests' do
    get 'absence_requests from company' do
      tags 'AbsenceRequests'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return absence_requests' do
        sign_in(:owner)
        schema(absence_requests_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create absence_request' do
      let(:params) do
        {
          absence_request: {
            note: 'foo',
            employee_id: create(:user, :employee, company_id: company_id).id,
            absence_reason_id: create(:absence_reason, company_id: company_id).id,
            from: '08:00 01.03.2021'.to_date.iso8601,
            to: '12:00 01.03.2021'.to_date.iso8601,
          },
        }
      end

      tags 'AbsenceRequests'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          absence_request: {
            properties: {
              note: { type: :string },
              employee_id: { type: :string },
              absence_reason_id: { type: :string },
              from: { type: :string },
              to: { type: :string },
            },
            required: %w[note absence_reason_id from to],
          },
          required: %w[absence_request],
        },
      }

      response '201', 'created absence_request' do
        sign_in(:owner)
        schema(absence_request_schema)

        run_test! do |response|
          expect(AbsenceRequest.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            absence_request: {
              f: 'd',
            },
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { from: 'can\'t be empty' },
        }

        run_test!
      end
    end
  end

  path '/api/v1/absence_requests/{id}' do
    let(:absence_request) { create(:absence_request, company: owner.company) }
    let(:id) { absence_request.id }

    get 'absence_request from company' do
      tags 'AbsenceRequests'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns absence_request' do
        sign_in(:owner)
        schema(absence_request_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates absence_request' do
      tags 'AbsenceRequests'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          absence_request: {
            properties: {
              note: { type: :string },
              employee_id: { type: :string },
              absence_reason_id: { type: :string },
              from: { type: :string },
              to: { type: :string },
              status: { type: :string },
            },
          },
          required: %w[absence_request],
        },
      }

      response '200', 'updates absence_request' do
        sign_in(:owner)
        schema(absence_request_schema)
        let(:params) do
          {
            absence_request: {
              note: 'pepegovich',
            },
          }
        end

        run_test! do
          expect(absence_request.reload.note).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            absence_request: {
              from: nil,
            },
          }
        end
        examples 'application/json' => {
          errors: { from: 'can\'t be blank' },
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys absence_request' do
      tags 'AbsenceRequests'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys absence_request' do
        sign_in(:owner)
        schema(absence_request_schema)

        run_test! do
          expect(AbsenceRequest.find_by(id: absence_request.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end

  path '/api/v1/absence_requests/{id}/cancel' do
    let(:absence_request) { create(:absence_request, company: owner.company, employee: employee) }
    let(:id) { absence_request.id }

    put 'cancells absence_request' do
      tags 'AbsenceRequests'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'cancells absence_request' do
        sign_in(:employee)
        schema(absence_request_schema)

        run_test! do
          expect(AbsenceRequests::CancelAbsenceRequest).not_to receive(:call)
        end
      end

      response '403', 'not authorized for action' do
        sign_in(:employee)

        before { absence_request.update(status: :accepted) }

        examples 'application/json' => {
          errors: { from: 'can\'t be blank' },
        }
        run_test! do
          expect(AbsenceRequests::CancelAbsenceRequest).not_to receive(:call)
        end
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end
  end
end
