require 'swagger_helper'

RSpec.describe 'Authentication API at /api/v1/auth' do
  let(:password) { 'Pepega123' }
  let(:user) { create(:user, password: password) }
  let(:email) { user.email }

  path '/api/v1/auth/sign_in' do
    post 'logs in' do
      tags 'Authentication'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string },
          password: { type: :string },
          pushy_token: { type: :string },
        },
        required: %w[email password],
      }

      response '200', 'logged in' do
        let(:params) do
          {
            email: email,
            password: password,
          }
        end

        context 'when without pushy token' do
          run_test! do
            expect(UserDevice.count).to eq(0)
          end
        end

        context 'when with pushy token' do
          let(:params) do
            {
              email: email,
              password: password,
              pushy_token: 'some-nice-token',
            }
          end

          run_test! do
            expect(UserDevice.count).to eq(1)
          end
        end
      end

      response '401', 'on invalid credentials does not log in' do
        let(:params) do
          {
            email: 'fdsafdsa',
            password: 'fdsvcxzvxcvdf',
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/auth/sign_out' do
    delete 'logs out' do
      tags 'Authentication'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          pushy_token: { type: :string },
        },
        required: %w[],
      }

      let(:params) { {} }

      response '200', 'logs out' do
        sign_in(:user)

        run_test!

        context 'when with pushy token' do
          let!(:user_devices) { create_list(:user_device, 2, user: user) }
          let(:params) do
            user_devices
            {
              pushy_token: user.devices.first.pushy_token,
            }
          end

          it 'removes user device' do
            expect(user.devices.count).to eq(1)
          end
        end
      end

      response '404', 'not logged in' do
        run_test!
      end
    end
  end
end
