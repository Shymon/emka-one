require 'swagger_helper'

RSpec.describe 'Users API at /api/v1/users' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  user_properties_schema = { # TODO: Shouln't it be let(:user...) ?
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        first_name: { type: :string },
        last_name: { type: :string },
        username: { type: :string },
        email: { type: :string }
      },
      required: %w[id email first_name last_name username]
    }
  }
  users_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: user_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  user_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: user_properties_schema,
        required: %w[id]
      }
    }
  }

  path '/api/v1/users' do
    get 'users from company' do
      tags 'Users'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return users' do
        sign_in(:owner)
        schema(users_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create user' do
      let(:params) do
        {
          user: {
            first_name: 'foo',
            last_name: 'bar',
            username: 'foobar',
            email: 'foo@bar.com',
            password: 'Pepega123'
          }
        }
      end

      tags 'Users'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            properties: {
              first_name: { type: :string },
              last_name: { type: :string },
              username: { type: :string },
              email: { type: :string },
              password: { type: :string }
            },
            required: %w[email first_name last_name username password]
          },
          required: %w[user]
        }
      }

      response '201', 'created user' do
        sign_in(:owner)
        schema(user_schema)

        run_test! do |response|
          expect(User.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            user: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { password: 'invalid' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/users/{id}' do
    let(:user) { create(:user, :employee, company: owner.company) }
    let(:id) { user.id }

    get 'user from company' do
      tags 'Users'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns user' do
        sign_in(:owner)
        schema(user_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates user' do
      tags 'Users'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          user: {
            properties: {
              first_name: { type: :string },
              last_name: { type: :string },
              username: { type: :string },
              email: { type: :string },
              password: { type: :string }
            }
          },
          required: %w[user]
        }
      }

      response '200', 'updates user' do
        sign_in(:owner)
        schema(user_schema)
        let(:params) do
          {
            user: {
              first_name: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(user.reload.first_name).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            user: {
              first_name: ''
            }
          }
        end
        examples 'application/json' => {
          errors: { first_name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys user' do
      tags 'Users'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys user' do
        sign_in(:owner)
        schema(user_schema)

        run_test! do
          expect(User.find_by(id: user.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
