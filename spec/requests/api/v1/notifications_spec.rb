require 'swagger_helper'

RSpec.describe 'Notifications API at /api/v1/notifications' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  notification_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        created_at: { type: :string },
        kind: { type: :string },
        metadata: { type: :object },
      },
      required: %w[id created_at kind metadata],
    },
  }
  notifications_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: notification_properties_schema,
          required: %w[id],
        },
      },
    },
  }
  notification_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: notification_properties_schema,
        required: %w[id],
      },
    },
  }

  path '/api/v1/notifications' do
    get 'notifications for user' do
      tags 'Notifications'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return user notifications' do
        sign_in(:employee)
        schema(notifications_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end
  end

  path '/api/v1/notifications/{id}/read' do
    let(:notification) { create(:notification_reception, user: employee) }
    let(:id) { notification.id }

    put 'reads notification' do
      tags 'Notifications'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'reads notification' do
        sign_in(:employee)
        schema(notification_schema)

        run_test! do
          expect(notification.reload.read_at).to be_present
        end
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end
  end
end
