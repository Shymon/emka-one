require 'swagger_helper'

RSpec.describe 'Materials API at /api/v1/materials' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  material_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        name: { type: :string },
        price: { type: :string },
        unit: { type: :string }
      },
      required: %w[id name price unit]
    }
  }
  materials_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: material_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  material_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: material_properties_schema,
        required: %w[id]
      }
    }
  }

  path '/api/v1/materials' do
    get 'materials from company' do
      tags 'Materials'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return materials' do
        sign_in(:owner)
        schema(materials_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create material' do
      let(:params) do
        {
          material: {
            name: 'foo',
            unit: 'piece',
            price: 6666.6
          }
        }
      end

      tags 'Materials'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          material: {
            properties: {
              name: { type: :string },
              unit: { type: :string },
              price: { type: :number },
            },
            required: %w[name unit price]
          },
          required: %w[material]
        }
      }

      response '201', 'created material' do
        sign_in(:owner)
        schema(material_schema)

        run_test! do |response|
          expect(Material.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            material: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/materials/{id}' do
    let(:material) { create(:material, company: owner.company) }
    let(:id) { material.id }

    get 'material from company' do
      tags 'Materials'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns material' do
        sign_in(:owner)
        schema(material_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates material' do
      tags 'Materials'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          material: {
            properties: {
              name: { type: :string },
              unit: { type: :string },
              price: { type: :number }
            }
          },
          required: %w[material]
        }
      }

      response '200', 'updates material' do
        sign_in(:owner)
        schema(material_schema)
        let(:params) do
          {
            material: {
              name: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(material.reload.name).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            material: {
              name: ''
            }
          }
        end
        examples 'application/json' => {
          errors: { name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys material' do
      tags 'Materials'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys material' do
        sign_in(:owner)
        schema(material_schema)

        run_test! do
          expect(Material.find_by(id: material.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
