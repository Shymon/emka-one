require 'swagger_helper'

RSpec.describe 'WorkEntries API at /api/v1/work_entries' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  work_entry_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        notes: { type: :string },
        day: { type: :string },
        construction_site_id: { type: :string },
        absence_reason_id: { type: :string, nullable: true },
        employee_id: { type: :string },
        spent_time: { type: :integer }
      },
      required: %w[
        id
        notes
        day
        construction_site_id
        absence_reason_id
        employee_id
        spent_time
      ]
    }
  }
  work_entries_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: work_entry_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  work_entry_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: work_entry_properties_schema,
        required: %w[id]
      }
    }
  }

  path '/api/v1/work_entries' do
    get 'work_entries from company' do
      tags 'WorkEntries'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return work_entries' do
        sign_in(:owner)
        schema(work_entries_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create work_entry' do
      let(:params) do
        {
          work_entry: {
            notes: 'foo',
            day: '29-07-2021',
            construction_site_id: create(:construction_site, company_id: company_id).id,
            spent_time: 331,
            employee_id: create(:user, :employee, company_id: company_id).id
          }
        }
      end

      tags 'WorkEntries'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          work_entry: {
            properties: {
              notes: { type: :string },
              day: { type: :string },
              construction_site_id: { type: :string },
              absence_reason_id: { type: :string },
              employee_id: { type: :string },
              spent_time: { type: :integer }
            },
            required: %w[spent_time]
          },
          required: %w[work_entry]
        }
      }

      response '201', 'created work_entry' do
        sign_in(:owner)
        schema(work_entry_schema)

        run_test! do |response|
          expect(WorkEntry.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            work_entry: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/work_entries/{id}' do
    let(:work_entry) { create(:work_entry, company: owner.company) }
    let(:id) { work_entry.id }

    get 'work_entry from company' do
      tags 'WorkEntries'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns work_entry' do
        sign_in(:owner)
        schema(work_entry_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates work_entry' do
      tags 'WorkEntries'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          work_entry: {
            properties: {
              notes: { type: :string },
              day: { type: :string },
              construction_site_id: { type: :string },
              absence_reason_id: { type: :string },
              employee_id: { type: :string },
              spent_time: { type: :integer }
            }
          },
          required: %w[work_entry]
        }
      }

      response '200', 'updates work_entry' do
        sign_in(:owner)
        schema(work_entry_schema)
        let(:params) do
          {
            work_entry: {
              notes: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(work_entry.reload.notes).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            work_entry: {
              spent_time: nil
            }
          }
        end
        examples 'application/json' => {
          errors: { spent_time: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys work_entry' do
      tags 'WorkEntries'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys work_entry' do
        sign_in(:owner)
        schema(work_entry_schema)

        run_test! do
          expect(WorkEntry.find_by(id: work_entry.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
