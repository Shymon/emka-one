require 'swagger_helper'

RSpec.describe 'EstimationRooms API at /api/v1/estimations/:id/estimation_rooms' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:client) { create(:client) }
  let(:company_id) { owner.company_id }
  let(:estimation) { create(:estimation, company: owner.company) }
  let(:estimation_id) { estimation.id }
  estimation_room_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        name: { type: :string },
        estimation_id: { type: :string },
      },
      required: %w[id name estimation_id]
    }
  }
  estimation_rooms_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: estimation_room_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  estimation_room_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: estimation_room_properties_schema,
        required: %w[id]
      }
    }
  }
  nested_estimation_room_attributes = {
    estimation_items_attributes: {
      type: :array,
      items: {
        type: :object,
        properties: {
          quantity: { type: :number },
          material_id: { type: :string }
        },
        required: %w[quantity material_id]
      }
    }
  }

  path '/api/v1/estimations/{estimation_id}/estimation_rooms' do
    get 'estimation_rooms from company' do
      tags 'EstimationRooms'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string

      response '200', 'return estimation_rooms' do
        sign_in(:owner)
        schema(estimation_rooms_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create estimation_room' do
      let(:material) { create(:material) }
      let(:params) do
        {
          estimation_room: {
            name: 'foo',
            client_id: client.id,
            estimation_items_attributes: [
              {
                quantity: 11.4,
                material_id: material.id
              }
            ]
          }
        }
      end

      tags 'EstimationRooms'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          estimation_room: {
            properties: {
              name: { type: :string },
              **nested_estimation_room_attributes
            },
            required: %w[name]
          },
          required: %w[estimation_room]
        }
      }

      response '201', 'created estimation_room' do
        sign_in(:owner)
        schema(estimation_room_schema)

        run_test! do |response|
          estimation_room = EstimationRoom.find(JSON.parse(response.body)['data']['id'])
          expect(estimation_room).to be_present
          expect(estimation_room.estimation_items.count).to eq(1)
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            estimation_room: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/estimations/{estimation_id}/estimation_rooms/{id}' do
    let(:estimation_room) { create(:estimation_room, estimation: estimation) }
    let(:id) { estimation_room.id }

    get 'estimation_room from company' do
      tags 'EstimationRooms'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns estimation_room' do
        sign_in(:owner)
        schema(estimation_room_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates estimation_room' do
      tags 'EstimationRooms'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          estimation_room: {
            properties: {
              name: { type: :string },
              **nested_estimation_room_attributes
            }
          },
          required: %w[estimation_room]
        }
      }

      response '200', 'updates estimation_room' do
        sign_in(:owner)
        schema(estimation_room_schema)
        let(:params) do
          {
            estimation_room: {
              name: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(estimation_room.reload.name).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            estimation_room: {
              name: ''
            }
          }
        end
        examples 'application/json' => {
          errors: { name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys estimation_room' do
      tags 'EstimationRooms'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys estimation_room' do
        sign_in(:owner)
        schema(estimation_room_schema)

        run_test! do
          expect(EstimationRoom.find_by(id: estimation_room.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
