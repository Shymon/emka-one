require 'swagger_helper'

RSpec.describe 'EstimationRooms/EstimationItems API at /api/v1/estimation/:id/estimation_rooms/:id/estimation_items' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:client) { create(:client) }
  let(:company_id) { owner.company_id }
  let(:estimation) { create(:estimation, company: owner.company) }
  let(:estimation_room) { create(:estimation_room, estimation: estimation) }
  let(:estimation_room_id) { estimation_room.id }
  let(:estimation_id) { estimation.id }
  estimation_item_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        quantity: { type: :string },
        material_id: { type: :string },
        estimation_room_id: { type: :string }
      },
      required: %w[id quantity material_id estimation_room_id]
    }
  }
  estimation_items_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: estimation_item_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  estimation_item_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: estimation_item_properties_schema,
        required: %w[id]
      }
    }
  }

  path '/api/v1/estimations/{estimation_id}/estimation_rooms/{estimation_room_id}/estimation_items' do
    get 'estimation_items from company' do
      tags 'EstimationItems'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :estimation_room_id, in: :path, type: :string

      response '200', 'return estimation_items' do
        sign_in(:owner)
        schema(estimation_items_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create estimation_item' do
      let(:material) { create(:material) }
      let(:params) do
        {
          estimation_item: {
            quantity: 909,
            material_id: material.id
          }
        }
      end

      tags 'EstimationItems'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :estimation_room_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          estimation_item: {
            properties: {
              quantity: { type: :string },
              material_id: { type: :string }
            },
            required: %w[quantity material_id]
          },
          required: %w[estimation_item]
        }
      }

      response '201', 'created estimation_item' do
        sign_in(:owner)
        schema(estimation_item_schema)

        run_test! do |response|
          expect(EstimationItem.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            estimation_item: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/estimations/{estimation_id}/estimation_rooms/{estimation_room_id}/estimation_items/{id}' do
    let(:estimation_item) { create(:estimation_item, estimation_room: estimation_room) }
    let(:id) { estimation_item.id }

    get 'estimation_item from company' do
      tags 'EstimationItems'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :estimation_room_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns estimation_item' do
        sign_in(:owner)
        schema(estimation_item_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates estimation_item' do
      tags 'EstimationItems'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :estimation_room_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          estimation_item: {
            properties: {
              quantity: { type: :string },
              material_id: { type: :string }
            }
          },
          required: %w[estimation_item]
        }
      }

      response '200', 'updates estimation_item' do
        sign_in(:owner)
        schema(estimation_item_schema)
        let(:new_quantity) { estimation_item.quantity + 1234 }
        let(:params) do
          {
            estimation_item: {
              quantity: new_quantity
            }
          }
        end

        run_test! do
          expect(estimation_item.reload.quantity).to eq(new_quantity)
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            estimation_item: {
              quantity: nil
            }
          }
        end
        examples 'application/json' => {
          errors: { name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys estimation_item' do
      tags 'EstimationItems'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :estimation_id, in: :path, type: :string
      parameter name: :estimation_room_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys estimation_item' do
        sign_in(:owner)
        schema(estimation_item_schema)

        run_test! do
          expect(EstimationItem.find_by(id: estimation_item.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
