require 'swagger_helper'

RSpec.describe 'ConstructionSites API at /api/v1/construction_sites' do
  let(:owner) { create(:user, :owner) }
  let(:employee) { create(:user, :employee, company: owner.company) }
  let(:company_id) { owner.company_id }
  construction_site_properties_schema = {
    id: { type: :string },
    attributes: {
      properties: {
        id: { type: :string },
        name: { type: :string },
        investment_time_from: { type: :string },
        investment_time_to: { type: :string },
        active: { type: :bool }
      },
      required: %w[id name]
    }
  }
  construction_sites_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :array,
        items: {
          properties: construction_site_properties_schema,
          required: %w[id]
        }
      }
    }
  }
  construction_site_schema = {
    type: :object,
    required: %w[data],
    properties: {
      data: {
        type: :object,
        properties: construction_site_properties_schema,
        required: %w[id]
      }
    }
  }

  path '/api/v1/construction_sites' do
    get 'construction_sites from company' do
      tags 'ConstructionSites'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string

      response '200', 'return construction_sites' do
        sign_in(:owner)
        schema(construction_sites_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    post 'create construction_site' do
      let(:params) do
        {
          construction_site: {
            name: 'foo',
            investment_time_from: '01.03.2021'.to_date.iso8601,
            investment_time_to: '01.04.2021'.to_date.iso8601,
            active: false
          }
        }
      end

      tags 'ConstructionSites'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          construction_site: {
            properties: {
              name: { type: :string },
              investment_time_from: { type: :string },
              investment_time_to: { type: :string },
              active: { type: :bool }
            },
            required: %w[name investment_time_from investment_time_to]
          },
          required: %w[construction_site]
        }
      }

      response '201', 'created construction_site' do
        sign_in(:owner)
        schema(construction_site_schema)

        run_test! do |response|
          expect(ConstructionSite.find(JSON.parse(response.body)['data']['id'])).to be_present
        end
      end

      response '401', 'not logged in' do
        run_test!
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            construction_site: {
              f: 'd'
            }
          }
        end
        schema(
          type: :object,
          required: %w[errors]
        )
        examples 'application/json' => {
          errors: { name: 'can\'t be empty' }
        }

        run_test!
      end
    end
  end

  path '/api/v1/construction_sites/{id}' do
    let(:construction_site) { create(:construction_site, company: owner.company) }
    let(:id) { construction_site.id }

    get 'construction_site from company' do
      tags 'ConstructionSites'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'returns construction_site' do
        sign_in(:owner)
        schema(construction_site_schema)

        run_test!
      end

      response '401', 'not logged in' do
        run_test!
      end
    end

    put 'updates construction_site' do
      tags 'ConstructionSites'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string
      parameter name: :params, in: :body, type: :object, schema: {
        type: :object,
        properties: {
          construction_site: {
            properties: {
              name: { type: :string },
              investment_time_from: { type: :string },
              investment_time_to: { type: :string },
              active: { type: :bool }
            }
          },
          required: %w[construction_site]
        }
      }

      response '200', 'updates construction_site' do
        sign_in(:owner)
        schema(construction_site_schema)
        let(:params) do
          {
            construction_site: {
              name: 'pepegovich'
            }
          }
        end

        run_test! do
          expect(construction_site.reload.name).to eq('pepegovich')
        end
      end

      response '422', 'invalid params' do
        sign_in(:owner)
        let(:params) do
          {
            construction_site: {
              name: ''
            }
          }
        end
        examples 'application/json' => {
          errors: { name: 'can\'t be blank' }
        }
        run_test!
      end

      response '401', 'not logged in' do
        let(:params) { {} }

        run_test!
      end
    end

    delete 'destroys construction_site' do
      tags 'ConstructionSites'
      produces 'application/json'
      consumes 'application/json'
      parameter name: :company_id, in: :path, type: :string
      parameter name: :id, in: :path, type: :string

      response '200', 'destroys construction_site' do
        sign_in(:owner)
        schema(construction_site_schema)

        run_test! do
          expect(ConstructionSite.find_by(id: construction_site.id)).to eq(nil)
        end
      end

      response '422', 'can\'t be destroyed'

      response '401', 'not logged in' do
        run_test!
      end
    end
  end
end
