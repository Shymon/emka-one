RSpec.shared_examples 'workspace_crud_unauthorized' do
  it 'redirects on unauthorized' do
    the_request

    expect(response.code.to_s).to eq('302')
  end
end

RSpec.shared_examples 'workspace_crud_200' do
  it 'returns 200' do
    sign_in(user, scope: :workspace_user)
    the_request

    expect(response.status.to_s).to eq('200')
  end
end

RSpec.shared_examples 'workspace_crud_redirect_to_index' do
  it 'redirects to index' do
    sign_in(user, scope: :workspace_user)
    the_request

    expect(response).to redirect_to("/workspace/#{url}")
  end
end

RSpec.shared_examples 'workspace_crud' do |desc_url|
  describe "GET workspace/#{desc_url}" do
    let(:the_request) { get "/workspace/#{url}" }

    include_examples 'workspace_crud_200'
    include_examples 'workspace_crud_unauthorized'
  end

  describe "GET workspace/#{desc_url}/new" do
    let(:the_request) { get "/workspace/#{url}/new" }

    include_examples 'workspace_crud_200'
    include_examples 'workspace_crud_unauthorized'
  end

  describe "GET workspace/#{desc_url}/{id}/edit" do
    let(:the_request) { get "/workspace/#{url}/#{record.id}/edit" }

    include_examples 'workspace_crud_200'
    include_examples 'workspace_crud_unauthorized'
  end

  describe "POST workspace/#{desc_url}" do
    let(:the_request) { post "/workspace/#{url}", params: create_params }

    include_examples 'workspace_crud_redirect_to_index'
    include_examples 'workspace_crud_unauthorized'
  end

  describe "PUT workspace/#{desc_url}/{id}" do
    let(:the_request) { put "/workspace/#{url}/#{record.id}", params: update_params }

    include_examples 'workspace_crud_redirect_to_index'
    include_examples 'workspace_crud_unauthorized'
  end

  describe "DELETE workspace/#{desc_url}/{id}" do
    let(:the_request) { delete "/workspace/#{url}/#{record.id}" }

    include_examples 'workspace_crud_redirect_to_index'
    include_examples 'workspace_crud_unauthorized'
  end
end
