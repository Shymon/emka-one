require 'rails_helper'

RSpec.describe WorkEntryPolicy, type: :policy do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:record) { build_stubbed(:work_entry, company: company) }
  let(:policy) { described_class.new(record, user: user) }

  describe '#index?' do
    subject { policy.apply(:index?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#show?' do
    subject { policy.apply(:show?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#create?' do
    subject { policy.apply(:create?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#update?' do
    subject { policy.apply(:update?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee and created entry' do
      before do
        update_role :employee
        record.employee = user
      end

      it { is_expected.to eq true }
    end

    context 'when the user is an employee but did not create entry' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe '#destroy?' do
    subject { policy.apply(:destroy?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee and created entry' do
      before do
        update_role :employee
        record.employee = user
      end

      it { is_expected.to eq true }
    end

    context 'when the user is an employee but did not create entry' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe 'relation scope' do
    let(:user) { create(:user, company: company) }
    let(:context) { { user: user } }

    before do
      create(:work_entry, notes: 'A', company: company, employee: user)
      create(:work_entry, notes: 'B', company: company)
      create(:work_entry, notes: 'C')
    end

    let(:target) do
      WorkEntry.where(notes: %w[A B C]).order(notes: :asc)
    end

    subject { policy.apply_scope(target, type: :active_record_relation).pluck(:notes) }

    context 'as user' do
      it { is_expected.to match_array(%w[]) }
    end

    context 'as owner' do
      before { update_role :owner }

      it { is_expected.to match_array(%w[A B]) }
    end

    context 'as employee' do
      before { update_role :employee }

      it { is_expected.to match_array(%w[A]) }
    end
  end

  describe 'params scope' do
    let(:user) { create(:user) }
    let(:context) { { user: user } }

    let(:params) do
      {
        notes: 'a',
        day: '29-07-2021',
        construction_site_id: 123,
        absence_reason_id: 321,
        spent_time: 331,
        employee_id: 123,
        company_id: 99
      }
    end
    let(:target) { ActionController::Parameters.new(params) }

    subject { policy.apply_scope(target, type: :action_controller_params).to_h }

    context 'as user' do
      it {
        is_expected.to eq(
          {}
        )
      }
    end

    context 'as owner' do
      before { update_role :owner }

      it {
        is_expected.to eq(
          {
            notes: 'a',
            day: '29-07-2021',
            construction_site_id: 123,
            absence_reason_id: 321,
            spent_time: 331,
            employee_id: 123
          }.with_indifferent_access
        )
      }
    end

    context 'as employee' do
      before { update_role :employee }

      it {
        is_expected.to eq(
          {
            notes: 'a',
            day: '29-07-2021',
            construction_site_id: 123,
            absence_reason_id: 321,
            spent_time: 331
          }.with_indifferent_access
        )
      }
    end
  end
end
