require 'rails_helper'

RSpec.describe MaterialPolicy, type: :policy do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:record) { build_stubbed(:material, company: company) }
  let(:policy) { described_class.new(record, user: user) }

  describe '#index?' do
    subject { policy.apply(:index?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#show?' do
    subject { policy.apply(:show?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#create?' do
    subject { policy.apply(:create?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe '#update?' do
    subject { policy.apply(:update?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe '#destroy?' do
    subject { policy.apply(:destroy?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe 'relation scope' do
    let(:user) { create(:user, company: company) }
    let(:context) { { user: user } }

    before do
      create(:material, name: 'A', company: company)
      create(:material, name: 'B')
    end

    let(:target) do
      Material.where(name: %w[A B]).order(name: :asc)
    end

    subject { policy.apply_scope(target, type: :active_record_relation).pluck(:name) }

    context 'as user' do
      it { is_expected.to match_array(%w[]) }
    end

    context 'as owner' do
      before { update_role :owner }

      it { is_expected.to match_array(%w[A]) }
    end

    context 'as employee' do
      before { update_role :employee }

      it { is_expected.to match_array(%w[]) }
    end
  end

  describe 'params scope' do
    let(:user) { create(:user) }
    let(:context) { { user: user } }

    let(:params) do
      {
        name: 'a',
        unit: :piece,
        price: 666,
        company_id: 99
      }
    end
    let(:target) { ActionController::Parameters.new(params) }

    subject { policy.apply_scope(target, type: :action_controller_params).to_h }

    context 'as user' do
      it {
        is_expected.to eq(
          {}
        )
      }
    end

    context 'as owner' do
      before { update_role :owner }

      it {
        is_expected.to eq(
          {
            name: 'a',
            unit: :piece,
            price: 666
          }.with_indifferent_access
        )
      }
    end

    context 'as employee' do
      before { update_role :employee }

      it {
        is_expected.to eq(
          {}.with_indifferent_access
        )
      }
    end
  end
end
