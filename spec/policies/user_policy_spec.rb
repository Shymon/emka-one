require 'rails_helper'

RSpec.describe UserPolicy, type: :policy do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:record) { build_stubbed(:user, company: company) }
  let(:policy) { described_class.new(record, user: user) }

  describe '#index?' do
    subject { policy.apply(:index?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      let(:user) { create(:user, :owner, company: company) }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      let(:user) { create(:user, :employee, company: company) }

      it { is_expected.to eq true }
    end
  end

  describe '#show?' do
    subject { policy.apply(:show?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      let(:user) { create(:user, :owner, company: company) }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      let(:user) { create(:user, :employee, company: company) }

      it { is_expected.to eq true }
    end
  end

  describe '#create?' do
    subject { policy.apply(:create?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      let(:user) { create(:user, :owner, company: company) }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      let(:user) { create(:user, :employee, company: company) }

      it { is_expected.to eq false }
    end
  end

  describe '#update?' do
    subject { policy.apply(:update?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      let(:user) { create(:user, :owner, company: company) }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee but updates different user' do
      let(:user) { create(:user, :employee, company: company) }

      it { is_expected.to eq false }
    end

    context 'when the user is an employee and updates self' do
      let(:user) { create(:user, :employee, company: company) }
      let(:record) { user }

      it { is_expected.to eq true }
    end
  end

  describe '#destroy?' do
    subject { policy.apply(:destroy?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      let(:user) { create(:user, :owner, company: company) }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      let(:user) { create(:user, :employee, company: company) }

      it { is_expected.to eq false }
    end
  end

  describe 'relation scope' do
    let(:user) { create(:user, username: 'SELF', company: company) }
    let(:context) { { user: user } }

    before do
      create(:user, username: 'A', company: company)
      create(:user, username: 'B')
    end

    let(:target) do
      User.where(username: %w[A B SELF]).order(username: :asc)
    end

    subject { policy.apply_scope(target, type: :active_record_relation).pluck(:username) }

    context 'as user' do
      it { is_expected.to match_array(%w[]) }
    end

    context 'as owner' do
      before { update_role :owner }

      it { is_expected.to match_array(%w[A SELF]) }
    end

    context 'as employee' do
      before { update_role :employee }

      it { is_expected.to match_array(%w[SELF]) }
    end
  end

  describe 'params scope' do
    let(:user) { create(:user) }
    let(:context) { { user: user } }

    let(:params) do
      {
        first_name: 'a',
        last_name: 'b',
        username: 'c',
        email: 'd@d.d',
        password: 'b',
        roles: [Role.find_or_initialize_by(name: Role.role(:owner))]
      }
    end
    let(:target) { ActionController::Parameters.new(params) }

    subject { policy.apply_scope(target, type: :action_controller_params).to_h }

    context 'as user' do
      it {
        is_expected.to eq(
          {
            password: 'b'
          }.with_indifferent_access
        )
      }
    end

    context 'as owner' do
      before { update_role :owner }

      it {
        is_expected.to eq(
          {
            first_name: 'a',
            last_name: 'b',
            username: 'c',
            email: 'd@d.d',
            password: 'b'
          }.with_indifferent_access
        )
      }
    end

    context 'as emplyee' do
      before { update_role :employee }

      it {
        is_expected.to eq(
          {
            password: 'b'
          }.with_indifferent_access
        )
      }
    end
  end
end
