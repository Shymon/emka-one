require 'rails_helper'

RSpec.describe EstimationItemPolicy, type: :policy do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:estimation) { create(:estimation, company: company, creator: user) }
  let(:estimation_room) { create(:estimation_room, estimation: estimation) }
  let(:record) { build_stubbed(:estimation_item, estimation_room: estimation_room) }
  let(:policy) { described_class.new(record, user: user) }

  describe '#index?' do
    subject { policy.apply(:index?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#show?' do
    subject { policy.apply(:show?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#create?' do
    subject { policy.apply(:create?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe '#update?' do
    subject { policy.apply(:update?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe '#destroy?' do
    subject { policy.apply(:destroy?) }

    it 'returns false when user has no role' do
      is_expected.to eq false
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq false }
    end
  end

  describe 'relation scope' do
    let(:user) { create(:user, company: company) }
    let(:context) { { user: user } }

    before do
      create(:estimation_item, quantity: 1, estimation_room: estimation_room)
      create(:estimation_item, quantity: 2)
    end

    let(:target) do
      EstimationItem.where(quantity: [1, 2]).order(quantity: :asc)
    end

    subject { policy.apply_scope(target, type: :active_record_relation).pluck(:quantity) }

    context 'as user' do
      it { is_expected.to match_array([]) }
    end

    context 'as owner' do
      before { update_role :owner }

      it { is_expected.to match_array([1]) }
    end

    context 'as employee' do
      before { update_role :employee }

      it { is_expected.to match_array([]) }
    end
  end

  describe 'params scope' do
    let(:user) { create(:user) }
    let(:context) { { user: user } }
    let(:material) { create(:material) }

    let(:params) do
      {
        quantity: 4321,
        material_id: material.id,
        estimation_room_id: 123
      }
    end
    let(:target) { ActionController::Parameters.new(params) }

    subject { policy.apply_scope(target, type: :action_controller_params).to_h }

    context 'as user' do
      it {
        is_expected.to eq(
          {}
        )
      }
    end

    context 'as owner' do
      before { update_role :owner }

      it {
        is_expected.to eq(
          {
            quantity: 4321,
            material_id: material.id
          }.with_indifferent_access
        )
      }
    end

    context 'as employee' do
      before { update_role :employee }

      it {
        is_expected.to eq(
          {}.with_indifferent_access
        )
      }
    end
  end
end
