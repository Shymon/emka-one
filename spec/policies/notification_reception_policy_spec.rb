require 'rails_helper'

RSpec.describe NotificationReceptionPolicy, type: :policy do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }
  let(:record) { build_stubbed(:notification_reception, user: user) }
  let(:policy) { described_class.new(record, user: user) }

  describe '#index?' do
    subject { policy.apply(:index?) }

    it 'returns false when user has no role' do
      is_expected.to eq true
    end

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe '#read?' do
    subject { policy.apply(:read?) }

    context 'when the user is owner' do
      before { update_role :owner }

      it { is_expected.to eq true }
    end

    context 'when the user is an employee' do
      before { update_role :employee }

      it { is_expected.to eq true }
    end
  end

  describe 'relation scope' do
    let(:user) { create(:user, company: company) }
    let(:context) { { user: user } }

    before do
      create(:notification_reception, push_id: 'asdf', user: user)
      create(:notification_reception, push_id: 'fdsa')
    end

    let(:target) do
      NotificationReception.where(push_id: ['asdf']).order(created_at: :asc)
    end

    subject { policy.apply_scope(target, type: :active_record_relation).pluck(:push_id) }

    context 'as user' do
      it { is_expected.to match_array(['asdf']) }
    end

    context 'as owner' do
      before { update_role :owner }

      it { is_expected.to match_array(['asdf']) }
    end

    context 'as employee' do
      before { update_role :employee }

      it { is_expected.to match_array(['asdf']) }
    end
  end
end
