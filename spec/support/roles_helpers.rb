module RolesHelpers
  def update_role(role_key)
    user.roles.destroy_all
    user.add_role Role.role(role_key)
  end
end
